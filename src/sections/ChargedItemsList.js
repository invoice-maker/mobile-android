import React from 'react';
import { ChargedItemStash } from '../components';
import { View } from 'react-native';
import { ListItem, Icon } from 'react-native-elements';
import tailwind, { getColor } from 'tailwind-rn';
import { useDispatch } from 'react-redux';
import { putChargedItemToTempStashToEdit, removeItemFromChargedItemByItemUid } from '../models/invoice/invoice-calculations.slice';
import {useSelector} from 'react-redux';

function OptionsComponent(props) {

    const { hideBottomSheet, item_uid } = props;

    const dispatch = useDispatch();

    function editCurrentItem() {
        dispatch(putChargedItemToTempStashToEdit({
            item_uid: item_uid
        }))

        hideBottomSheet()
    }

    function removeCurrentItem() {
        dispatch(removeItemFromChargedItemByItemUid({
            item_uid: item_uid
        }))
    }


    return (
        <>
            <ListItem bottomDivider onPress={editCurrentItem}>
                <Icon
                    name='edit'
                    type='ant-design'
                    color={getColor('blue-600')}
                />
                <ListItem.Content>
                    <ListItem.Title style={tailwind('text-base')}>Edit item</ListItem.Title>
                </ListItem.Content>
                <ListItem.Chevron />
            </ListItem>

            <ListItem bottomDivider onPress={removeCurrentItem}>
                <Icon
                    name='delete'
                    type='ant-design'
                    color={getColor('red-600')}
                />
                <ListItem.Content>
                    <ListItem.Title style={tailwind('text-base')}>Delete item</ListItem.Title>
                </ListItem.Content>
                <ListItem.Chevron />
            </ListItem>
        </>
    )
}

export default function ChargedItemsList() {


    const charged_items = useSelector(state => state.invoiceCalculations.charged_items);


    return (
        charged_items.map(item => {
            return (
                <View style={tailwind('my-2')} key={item.item_uid}>
                    <ChargedItemStash
                        {...item}
                        withOptions={true}
                        optionsTitle='Item options'
                        item_uid={item.item_uid}
                        OptionsComponent={OptionsComponent}
                    />
                </View>
            )

        })
    )
}