import React from 'react';
import { useDispatch } from 'react-redux';
import tailwind, { getColor } from 'tailwind-rn';
import { ListItem, Icon } from 'react-native-elements';
import {
    removeItemFromCalcStash
} from '../models/invoice/invoice-calculations.slice';


export default function OptionsComponent(props) {

    const {name}  = props

    const { hideBottomSheet, editButtonHandler, showButtonOnToggleButton } = props;

    const dispatch = useDispatch();

    function editCurrentItem() {
        editButtonHandler()

        hideBottomSheet()
    }

    function removeCurrentItem() {
        dispatch(removeItemFromCalcStash({
            name: name
        }))

        showButtonOnToggleButton()
        
        hideBottomSheet()
    }


    return (
        <>
            <ListItem bottomDivider
                onPress={editCurrentItem}
            >
                <Icon
                    name='edit'
                    type='ant-design'
                    color={getColor('blue-600')}
                />
                <ListItem.Content>
                    <ListItem.Title style={tailwind('text-base')}>Edit item</ListItem.Title>
                </ListItem.Content>
                <ListItem.Chevron />
            </ListItem>

            <ListItem bottomDivider
                onPress={removeCurrentItem}
            >
                <Icon
                    name='delete'
                    type='ant-design'
                    color={getColor('red-600')}
                />
                <ListItem.Content>
                    <ListItem.Title style={tailwind('text-base')}>Delete item</ListItem.Title>
                </ListItem.Content>
                <ListItem.Chevron />
            </ListItem>
        </>
    )
}