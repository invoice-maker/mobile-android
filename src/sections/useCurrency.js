import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';

export default function useCurrency() {
     
    const predefined_currency_symbol = useSelector(state => state.invoiceCalculations.currency_symbol)
    const custom_currency_symbol = useSelector(state => state.invoiceCalculations.custom_currency_symbol)

    const [currency_symbol, setCurrencySymbol] = useState(null)

    useEffect(() => {
        if (custom_currency_symbol) {
            setCurrencySymbol(custom_currency_symbol)
        } else {
            setCurrencySymbol(predefined_currency_symbol)
        }
    }, [predefined_currency_symbol, custom_currency_symbol])

    return [
        currency_symbol,
        setCurrencySymbol
    ]
}