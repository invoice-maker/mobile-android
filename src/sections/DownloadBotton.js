import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import tailwind from 'tailwind-rn';
import { generatePDFinvoice } from '../models/invoice/invoice-generators.slice';

function useGenerateCreateInvoiceJSONPayload() {

    const invoiceLabels = useSelector(state => state.invoiceLabels)
    const invoiceReferences = useSelector(state => state.invoiceReferences)
    const invoiceCalculations = useSelector(state => state.invoiceCalculations)
    const invoiceInclusions = useSelector(state => state.invoiceInclusions)
    const invoiceExportOptions = useSelector(state => state.invoiceExportOptions)

    const payload = {
        export_options: invoiceExportOptions,
        document_labels: invoiceLabels,
        document_inclusions: invoiceInclusions,
        document_references: invoiceReferences,
        document_body: {
            currency_format: {
                number_format: invoiceCalculations.number_format,
                currency_code: invoiceCalculations.currency_code,
                currency_symbol: invoiceCalculations.currency_symbol,
                currency_symbol_on_left: invoiceCalculations.currency_symbol_on_left,
                custom_currency_simbol: invoiceCalculations.custom_currency_simbol
            },
            charged_items: invoiceCalculations.charged_items,
            calculation_components: invoiceCalculations.invoice_calculation

        }
    }

    return payload

}


export default function DownloadBotton() {

    const navigation = useNavigation()
    const payload = useGenerateCreateInvoiceJSONPayload()

    const dispatch = useDispatch()

    const generateInvoiceHandler = () => {
        dispatch(generatePDFinvoice(payload))
        navigation.navigate('InvoiceLoading')
    }

    return (
        <View
            style={tailwind('m-4')}
        >

            <Button
                onPress={generateInvoiceHandler}
                buttonStyle={tailwind('bg-yellow-600')}
                title="Generate Invoice"
            />
        </View>
    )
}