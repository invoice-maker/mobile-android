import React, { useEffect } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import tailwind from 'tailwind-rn';
import { Icon, Button } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import * as WebBrowser from 'expo-web-browser';


export default function InvoiceLoadingPage() {

    const invoiceLoadingState = useSelector(state => state.invoiceGenerators.generate_invoice_loading)
    const downloadUrl = useSelector(state => state.invoiceGenerators.download_url)


    const navigattion = useNavigation()

    const openInBrowserhandler = () => {
        WebBrowser.openBrowserAsync(downloadUrl, {
            enableDefaultShare: true
        });
    }

    useEffect(() => {

        if ((invoiceLoadingState === 'fulfilled') && (downloadUrl !== null)) {
            openInBrowserhandler()
        }

    }, [invoiceLoadingState])

    const Loading = (
        <>
            <ActivityIndicator size={100} color="#ffffff" />
            <Text style={tailwind('text-white text-lg my-8 text-center')}>
                Please wait your document is being generated
            </Text>
        </>
    )


    const Success = (
        <>
            <Icon
                size={100}
                name='check'
                type='octicons'
                color='#ffffff'
            />

            <Text style={tailwind('text-white text-lg my-4 text-center')}>
                Your document is ready
            </Text>

            <Button
                onPress={openInBrowserhandler}
                containerStyle={tailwind('my-4')}
                buttonStyle={tailwind('bg-white')}
                titleStyle={tailwind('text-blue-500')}
                title='Open now'
            />

        </>
    )

    const Failed = (

        <>
            <Icon
                size={100}
                name='emoji-sad'
                type='entypo'
                color='#ffffff'
            />

            <Text style={tailwind('text-white text-lg my-4 text-center')}>
                Sorry something is wrong {'\n'}
                Failed to generate your document
             </Text>
        </>

    )

    const CloseButton = (
        <Icon
            onPress={_ => navigattion.goBack()}
            containerStyle={tailwind('absolute top-0 right-0 m-4')}
            size={27}
            name='closecircleo'
            type='ant-design'
            color='#ffffff'
        />
    )

    return (

        <View style={tailwind('h-full w-full flex items-center justify-center bg-blue-600')}>

            {invoiceLoadingState !== 'pending' ? CloseButton : null}

            {invoiceLoadingState === 'pending' ? Loading : null}

            {invoiceLoadingState === 'fulfilled' ? Success : null}

            {invoiceLoadingState === 'rejected' ? Failed : null}

        </View>


    )
}