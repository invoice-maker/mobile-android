import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { InputText, OptionalFormCard, CheckboxField, EditModeBadge } from '../components';
import useDecimalInput from '../utility/useDecimalInput';
import { View } from 'react-native';
import tailwind from 'tailwind-rn';
import {
    setValueOnCalcStash,
    putItemFromCalcStashToCalculation,
} from '../models/invoice/invoice-calculations.slice';
import useCurrency from './useCurrency';



export default function ShippingCostForm(props) {

    const { hideForm, hideToggleButton } = props;

    const dispatch = useDispatch()

    const shipping_stash = useSelector(state => state.invoiceCalculations.calc_stash.shipping_cost);

    const [shippingCostValue, setShippingCostValue] = useDecimalInput(shipping_stash.value)

    const [isTaxable, setIsTaxable] = useState(shipping_stash.taxable)

    const setValueOnCalcStashHandler = (name, properties, value) => dispatch(setValueOnCalcStash({ name, properties, value }));
    const toggleTaxableHandler = () => setIsTaxable(!isTaxable)

    const putItemFromCalcStashToCalculationHandler = (name) => dispatch(putItemFromCalcStashToCalculation({ name }));

    const [currencySymbol, setCurrencySymbol] = useCurrency();


    useEffect(() => {
        setValueOnCalcStashHandler('shipping_cost', 'value', shippingCostValue.floatValue)
    }, [shippingCostValue])

    useEffect(() => {
        setValueOnCalcStashHandler('shipping_cost', 'taxable', isTaxable)
    }, [isTaxable])

    return (
        <OptionalFormCard
            title={shipping_stash.enabled ? 'Edit shipping cost' : 'Add shipping cost'}
            mainButtonText={shipping_stash.enabled ? 'Save' : 'Add cost'}
            onCancel={() => {
                hideForm()
                if (shipping_stash.enabled) {
                    hideToggleButton()
                }
            }}
            onSubmit={() => {
                putItemFromCalcStashToCalculationHandler('shipping_cost');
                hideToggleButton()
                hideForm()
            }}
        >


            {
                shipping_stash.enabled ?
                    <EditModeBadge containerStyle={tailwind('mb-4')} /> : null
            }



            <View style={tailwind('flex flex-col justify-between')}>


                <InputText
                    label={`Shipping cost (${currencySymbol})`}
                    onChangeText={(text) => setShippingCostValue({ value: text })}
                    value={shippingCostValue.stringValue}
                />

                <View style={tailwind('mb-2')}>
                    <CheckboxField
                        title='Set shipping is taxable'
                        checked={isTaxable}
                        onPress={toggleTaxableHandler}
                    />
                </View>

            </View>


        </OptionalFormCard>

    )
}