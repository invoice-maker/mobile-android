import React from 'react';
import { Card, InputText, InputDatepicker, InputDatepickerWithoption, BottomSheetSwitch } from '../components';
import { setStringValue, setDateValue, setDueDateSpan } from '../models/invoice/invoice-references.slice';
import { useDispatch, useSelector } from 'react-redux';
import dayjs from 'dayjs';
import { setInclusionValue } from '../models/invoice/invoice-inclusions.slice';

function OptionsComponent() {

    const dispatch = useDispatch()

    const payment_terms = useSelector(state => state.invoiceInclusions.payment_terms);

    const due_date = useSelector(state => state.invoiceInclusions.due_date);

    const onPaymentTermsChangeHandler = (value) => dispatch(setInclusionValue({ name: 'payment_terms', value }));

    const onDueDateChangeHandler = (value) => dispatch(setInclusionValue({ name: 'due_date', value }));

    return (
        <>
            <BottomSheetSwitch
                title='Enable payment terms'
                subtitle='Include payment terms section on the invoice'
                onValueChange={onPaymentTermsChangeHandler}
                value={payment_terms}
            />
            <BottomSheetSwitch
                title='Enable invoice due date'
                subtitle='Include due date section on the invoice'
                onValueChange={onDueDateChangeHandler}
                value={due_date}
            />
        </>
    )
}

export default function DocumentReferenceSection(props) {

    const dispatch = useDispatch();

    const invoice_number_label = useSelector(state => state.invoiceLabels.invoice_number_label);
    const invoice_date_label = useSelector(state => state.invoiceLabels.invoice_date_label);
    const invoice_due_date_label = useSelector(state => state.invoiceLabels.invoice_due_date_label);
    const payment_terms_label = useSelector(state => state.invoiceLabels.payment_terms_label);


    const invoice_number = useSelector(state => state.invoiceReferences.invoice_number);
    const payment_terms = useSelector(state => state.invoiceReferences.payment_terms);
    const invoice_date = useSelector(state => state.invoiceReferences.invoice_date);
    const invoice_due_date = useSelector(state => state.invoiceReferences.invoice_due_date);

    const payment_terms_inclusion = useSelector(state => state.invoiceInclusions.payment_terms);

    const due_date_inclusion = useSelector(state => state.invoiceInclusions.due_date);

    const updateStringReferences = (text, name) => dispatch(setStringValue({
        name,
        value: text
    }))

    const updateInvoiceDate = (selectedDate) => dispatch(setDateValue({
        name: 'invoice_date',
        value: dayjs(selectedDate).valueOf()
    }))

    const updateDueDateSpan = (itemValue) => dispatch(setDueDateSpan({
        name: 'invoice_due_date',
        value: itemValue
    }))

    const updateDueDate = (selectedDate) => dispatch(setDateValue({
        name: 'invoice_due_date',
        value: dayjs(selectedDate).valueOf()
    }))



    return (

        <Card
            title="Invoice Reference"
            divider
            withOption={true}
            optionsTitle='Invoice Reference Options'
            OptionsComponent={OptionsComponent}
        >

            <InputText
                onChangeText={(text) => updateStringReferences(text, 'invoice_number')}
                label={invoice_number_label}
                value={invoice_number}
            />

            <InputDatepicker
                label={invoice_date_label}
                value={invoice_date}
                onChange={updateInvoiceDate}
            />

            { payment_terms_inclusion ?
                <InputText
                    onChangeText={(text) => updateStringReferences(text, 'payment_terms')}
                    label={payment_terms_label}
                    value={payment_terms}
                /> : null
            }


            {
                due_date_inclusion ?
                    <InputDatepickerWithoption
                        label={invoice_due_date_label}
                        value={invoice_due_date}
                        onSelectValueChange={updateDueDateSpan}
                        onChange={updateDueDate}
                        hideDatepicker={invoice_due_date.due_date_span === 'none' ? true : false}
                        datepickerDisabled={invoice_due_date.due_date_span === 'custom' ? false : true}
                        select={{
                            selected: invoice_due_date.due_date_span,
                            options: [
                                {
                                    value: 'none',
                                    label: "None"
                                },
                                {
                                    value: '7d',
                                    label: '7 days'
                                },
                                {
                                    value: '14d',
                                    label: '14 days'
                                },
                                {
                                    value: '1M',
                                    label: '1 month'
                                },
                                {
                                    value: 'custom',
                                    label: 'Custom'
                                }
                            ]
                        }}
                    /> : null
            }


        </Card>
    )
}