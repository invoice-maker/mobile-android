import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import tailwind, { getColor } from 'tailwind-rn';
import { Card, InputText, CalculationItemCard, OptionalFormCard } from '../components';
import { ButtonGroup } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import GlobalDiscountForm from './GlobalDiscountForm';
import InvoiceCalculationItemOptions from './InvoiceClaculationItemOptions';
import ShippingCostForm from './ShppingCostForm';
import TaxForm from './TaxForm';
import { calculateBalanceDue } from '../models/invoice/invoice-calculations.slice';
import('@formatjs/intl-numberformat/polyfill')
import '@formatjs/intl-getcanonicallocales/polyfill'
import '@formatjs/intl-locale/polyfill'
import '@formatjs/intl-pluralrules/polyfill'
// import '@formatjs/intl-pluralrules/polyfill-locales'
import '@formatjs/intl-numberformat/locale-data/tg' 
//
// import 'intl/locale-data/jsonp/id-ZA';

export default function CalculationSection(props) {

    const { parentScrollViewRef } = props

    const dispatch = useDispatch()

    const GlobalDiscountOptions = InvoiceCalculationItemOptions
    const TaxOptions = InvoiceCalculationItemOptions

    const subtotal = useSelector(state => state.invoiceCalculations.invoice_calculation.invoice_subtotal);
    const global_discount_calc = useSelector(state => state.invoiceCalculations.invoice_calculation.global_invoice_discount);
    const tax_calc = useSelector(state => state.invoiceCalculations.invoice_calculation.tax);
    const shipping_calc = useSelector(state => state.invoiceCalculations.invoice_calculation.shipping_cost);
    const balance_due_calc = useSelector(state => state.invoiceCalculations.invoice_calculation.balance_due);

    const subtotal_label = useSelector(state => state.invoiceLabels.subtotal_label)
    const global_discount_label = useSelector(state => state.invoiceLabels.global_discount_label)
    const tax_label = useSelector(state => state.invoiceLabels.tax_label)
    const shipping_label = useSelector(state => state.invoiceLabels.shipping_label)
    const balance_due_label = useSelector(state => state.invoiceLabels.balance_due_label)

    const [currentLayout, setCurrentlayout] = useState(null)
    const [formLayout, setFormLayout] = useState(null)
    const [activeForm, setActiveForm] = useState(null)
    const [predifenedOptionField, setPredifenedOptionField] = useState(['+ Discount', '+ Tax', '+ Shipping'])

    // const [balanceDueValue, setBalanceDueValue] = useState(balance_due_calc)
    const [isGlobalDiscountFormButtonVisible, setIsGlobalDiscountFormButtonVisible] = useState(true)
    const [isTaxFormButtonVisible, setisTaxFormButtonVisible] = useState(true)
    const [isShippingFormButtonVisible, setIsShippingFormButtonVisible] = useState(true)


    const hideForm = () => setActiveForm(null);
    const scrollSreenToForm = () => parentScrollViewRef.current.scrollTo({ y: (currentLayout.y + currentLayout.height + formLayout.y), animated: true })


    const editGlobalDiscountButtonHandler = () => {
        setIsGlobalDiscountFormButtonVisible(true)
        setActiveForm('+ Discount')
    }

    const editTaxButtonHandler = () => {
        setisTaxFormButtonVisible(true)
        setActiveForm('+ Tax')
    }

    const editShippingButtonHandler = () => {
        setIsShippingFormButtonVisible(true)
        setActiveForm('+ Shipping')
    }


    useEffect(() => {
        dispatch(calculateBalanceDue())
    }, [subtotal, global_discount_calc, tax_calc, shipping_calc, balance_due_calc])


    useEffect(() => {

        // scroll to form when button group clicked
        if (activeForm !== null) {
            scrollSreenToForm()
        }

    }, [activeForm])



    useEffect(() => {

        let predifenedOptions = []

        isGlobalDiscountFormButtonVisible ? predifenedOptions.push('+ Discount') : null
        isTaxFormButtonVisible ? predifenedOptions.push('+ Tax') : null
        isShippingFormButtonVisible ? predifenedOptions.push('+ Shipping') : null

        setPredifenedOptionField(predifenedOptions)

    }, [isGlobalDiscountFormButtonVisible, isTaxFormButtonVisible, isShippingFormButtonVisible])



    const PredefinedOptionsComponents = [
        <GlobalDiscountForm
            hideForm={hideForm}
            hideToggleButton={() => setIsGlobalDiscountFormButtonVisible(false)}
        />,
        <TaxForm
            hideForm={hideForm}
            hideToggleButton={() => setisTaxFormButtonVisible(false)}
        />,
        <ShippingCostForm
            hideForm={hideForm}
            hideToggleButton={() => setIsShippingFormButtonVisible(false)}
        />,
    ]



    return (

        <Card title='Invoice Calculation' divider onLayout={(e) => setCurrentlayout(e.nativeEvent.layout)}>

            <View>

                <View style={tailwind('flex flex-row py-1 justify-between')}>
                    <Text style={tailwind('w-1/2 font-bold text-base')}>
                        {subtotal_label}
                </Text>

                    <Text style={tailwind('font-bold text-base')}>
                        {subtotal || 0}
                    </Text>

                </View>

                <View>

                    {
                        global_discount_calc.enabled ?
                            <CalculationItemCard
                                name='global_invoice_discount'
                                showButtonOnToggleButton={() => setIsGlobalDiscountFormButtonVisible(true)}
                                editButtonHandler={editGlobalDiscountButtonHandler}
                                optionsTitle='Discount options'
                                withOptions={true}
                                label={global_discount_label}
                                value={global_discount_calc.value}
                                OptionsComponent={GlobalDiscountOptions}
                            >

                            </CalculationItemCard>
                            : null

                    }

                    {
                        (shipping_calc.enabled && shipping_calc.taxable === true) ?
                            <CalculationItemCard
                                name='shipping_cost'
                                showButtonOnToggleButton={() => setIsShippingFormButtonVisible(true)}
                                editButtonHandler={editShippingButtonHandler}
                                optionsTitle='Shipping cost options'
                                withOptions={true}
                                label={shipping_label}
                                value={shipping_calc.value}
                                OptionsComponent={InvoiceCalculationItemOptions}
                            >

                            </CalculationItemCard>
                            : null

                    }

                    {
                        tax_calc.enabled ?
                            <CalculationItemCard
                                name='tax'
                                showButtonOnToggleButton={() => setisTaxFormButtonVisible(true)}
                                editButtonHandler={editTaxButtonHandler}
                                optionsTitle='Tax options'
                                withOptions={true}
                                label={tax_label}
                                value={tax_calc.value}
                                OptionsComponent={TaxOptions}
                            >

                            </CalculationItemCard>
                            : null

                    }



                    {
                        (shipping_calc.enabled && shipping_calc.taxable === false) ?
                            <CalculationItemCard
                                name='shipping_cost'
                                showButtonOnToggleButton={() => setIsShippingFormButtonVisible(true)}
                                editButtonHandler={editShippingButtonHandler}
                                optionsTitle='Shipping cost options'
                                withOptions={true}
                                label={shipping_label}
                                value={shipping_calc.value}
                                OptionsComponent={InvoiceCalculationItemOptions}
                            >

                            </CalculationItemCard>
                            : null

                    }
                </View>


                <View style={tailwind('flex flex-row mt-4 mb-4 justify-between border-t border-b py-2 border-blue-300')}>
                    <Text style={tailwind('w-1/2 font-bold text-lg text-black')}>
                        {balance_due_label}
                    </Text>

                    <Text style={tailwind('font-bold text-lg text-black')}>
                        {balance_due_calc}
                        {/* { Intl.NumberFormat('tg').format(123456789.654987) } */}
                    </Text>
                </View>

                {predifenedOptionField.length > 0 ?
                    <ButtonGroup
                        onPress={(e) => setActiveForm(predifenedOptionField[e])}
                        containerStyle={{ ...tailwind('m-0'), borderColor: getColor('blue-400') }}
                        innerBorderStyle={{ color: getColor('blue-500') }}
                        textStyle={tailwind('text-base text-blue-500')}
                        selectedIndex={(predifenedOptionField.findIndex(item => item === activeForm))}
                        selectedButtonStyle={tailwind('bg-blue-500')}
                        buttons={predifenedOptionField}
                    />
                    : null
                }

                <View onLayout={(e) => setFormLayout(e.nativeEvent.layout)} >

                    {activeForm === '+ Discount' ? PredefinedOptionsComponents[0] : null}

                    {activeForm === '+ Tax' ? PredefinedOptionsComponents[1] : null}

                    {activeForm === '+ Shipping' ? PredefinedOptionsComponents[2] : null}

                </View>



            </View>

        </Card>

    )
}