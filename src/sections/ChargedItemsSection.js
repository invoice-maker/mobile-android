import React, { useEffect, useRef, useState } from 'react';
import { Text, View } from 'react-native';
import tailwind from 'tailwind-rn';
import { Card, InputText, InputSelect, TotalLabel, InputTextArea, BottomSheetSwitch } from '../components';
import { Button } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import useDecimalInput from '../utility/useDecimalInput';
import {
    setStringValueToTempStash,
    setFloatValueToTempStash,
    calculateTempStashAmount,
    putItemOnTempStashToChargedItems,
    cancelEditChargedItem,
    updateChargedItemByItemId
} from '../models/invoice/invoice-calculations.slice';
import ChargedItemsList from './ChargedItemsList';
import { setInclusionValue } from '../models/invoice/invoice-inclusions.slice';
import  useCurrency  from './useCurrency';

function OptionsComponent() {

    const dispatch = useDispatch()

    const item_description = useSelector(state => state.invoiceInclusions.item_description);

    const item_discount = useSelector(state => state.invoiceInclusions.item_discount);

    const item_qty = useSelector(state => state.invoiceInclusions.item_qty);

    const onItemDescriptionChangeHandler = (value) => dispatch(setInclusionValue({ name: 'item_description', value }));

    const onItemDiscountChangeHandler = (value) => dispatch(setInclusionValue({ name: 'item_discount', value }));

    const onItemQtyChangeHandler = (value) => dispatch(setInclusionValue({ name: 'item_qty', value }));

    return (
        <>
            <BottomSheetSwitch
                title='Enable item description'
                subtitle='Include item description section on the invoice'
                onValueChange={onItemDescriptionChangeHandler}
                value={item_description}
            />
            <BottomSheetSwitch
                title='Enable item quantity'
                subtitle='Include item quantity column on the invoice'
                onValueChange={onItemQtyChangeHandler}
                value={item_qty}
            />
            <BottomSheetSwitch
                title='Enable discount per item basis'
                subtitle='Include item discount column on the invoice'
                onValueChange={onItemDiscountChangeHandler}
                value={item_discount}
            />
        </>
    )
}

export default function ChargedItemsSection(props) {



    const edit_mode = useSelector(state => state.invoiceCalculations.edit_mode);
    const item_uid = useSelector(state => state.invoiceCalculations.temp_stash.item_uid);
    const item_name = useSelector(state => state.invoiceCalculations.temp_stash.item_name);
    const item_description = useSelector(state => state.invoiceCalculations.temp_stash.item_description);
    const item_qty = useSelector(state => state.invoiceCalculations.temp_stash.item_qty);
    const item_price = useSelector(state => state.invoiceCalculations.temp_stash.item_price);
    const item_discount_type = useSelector(state => state.invoiceCalculations.temp_stash.item_discount_type);
    const item_discount = useSelector(state => state.invoiceCalculations.temp_stash.item_discount);
    const item_amount = useSelector(state => state.invoiceCalculations.temp_stash.item_amount);

    const item_description_inclusion = useSelector(state => state.invoiceInclusions.item_description);
    const item_discount_inclusion = useSelector(state => state.invoiceInclusions.item_discount);
    const item_qty_inclusion = useSelector(state => state.invoiceInclusions.item_qty);

    const item_description_label = useSelector(state => state.invoiceLabels.item_description_label)
    const item_qty_label = useSelector(state => state.invoiceLabels.item_qty_label)
    const item_price_label = useSelector(state => state.invoiceLabels.item_price_label)
    const item_discount_label = useSelector(state => state.invoiceLabels.item_discount_label)
    const item_amount_label = useSelector(state => state.invoiceLabels.item_amount_label)

    const [currency_symbol, setCurrencySymbol] = useCurrency()

    const { parentScrollViewRef } = props

    const [currentLayout, setCurrentLayout] = useState()

    const dispatch = useDispatch()

    const [qty, setQty] = useDecimalInput(item_qty)

    const [price, setPrice] = useDecimalInput(item_price)

    const [discountValue, setDiscountValue] = useDecimalInput(item_discount)


    const updateStringValueToTempStash = (text, name) => dispatch(setStringValueToTempStash({
        name: name,
        value: text
    }))

    const updateFloatValueToTempStash = (text, name) => dispatch(setFloatValueToTempStash({
        name: name,
        value: text
    }))



    useEffect(() => {
        setQty({ value: item_qty })
        setPrice({ value: item_price })
        setDiscountValue({ value: item_discount })

        dispatch(calculateTempStashAmount())

        parentScrollViewRef.current.scrollTo({ ...currentLayout, animated: true })

    }, [item_uid])

    useEffect(() => {
        updateFloatValueToTempStash(qty.floatValue, 'item_qty')
        dispatch(calculateTempStashAmount())
    }, [qty])

    useEffect(() => {
        updateFloatValueToTempStash(price.floatValue, 'item_price')
        dispatch(calculateTempStashAmount())
    }, [price])

    useEffect(() => {
        updateFloatValueToTempStash(discountValue.floatValue, 'item_discount')
        dispatch(calculateTempStashAmount())
    }, [discountValue])

    useEffect(() => {
        dispatch(calculateTempStashAmount())
    }, [item_discount_type])


    function resetFloatValues() {
        setQty({ value: 0 })
        setPrice({ value: 0 })
        setDiscountValue({ value: 0 })
    }


    let formActionButton
    if (!edit_mode) {
        formActionButton = (
            <View>
                <Button
                    onPress={_ => {
                        dispatch(putItemOnTempStashToChargedItems())
                        resetFloatValues()
                    }}
                    title="Add item"
                />
            </View>
        )
    } else {
        formActionButton = (
            <View style={tailwind('flex flex-row justify-between')}>
                <Button
                    containerStyle={{ width: '45%' }}
                    onPress={_ => {
                        dispatch(cancelEditChargedItem())
                        resetFloatValues()
                    }}
                    title="Cancel Edit"
                    type='outline'
                />
                <Button
                    containerStyle={{ width: '45%' }}
                    onPress={_ => {
                        dispatch(updateChargedItemByItemId({
                            item_uid: item_uid
                        }))
                        resetFloatValues()
                    }}
                    title="Save Edit"
                />
            </View>
        )
    }



    return (
        <Card
            title="Charged Items"
            divider
            onLayout={(e) => setCurrentLayout(e.nativeEvent.layout)}
            withOption={true}
            optionsTitle='Charged Items Options'
            OptionsComponent={OptionsComponent}
        >

            {edit_mode ?
                <View style={tailwind('flex flex-row items-center justify-center')}>
                    <Text style={tailwind('py-1 px-2 bg-yellow-200 rounded-full text-base text-center')}>Edit mode</Text>
                </View> : null
            }

            <View style={tailwind('rounded p-1')}>
                <InputText
                    onChangeText={(text) => updateStringValueToTempStash(text, 'item_name')}
                    label={item_description_label}
                    value={item_name}
                />

                {item_description_inclusion ?
                    <InputTextArea
                        onChangeText={(text) => updateStringValueToTempStash(text, 'item_description')}
                        label="Item Description"
                        value={item_description}
                        numberOfLines={3}
                    /> : null
                }

                <View style={tailwind('flex flex-row justify-between')}>

                    {
                        item_qty_inclusion ?
                            <InputText
                                containerStyle={{ width: '30%', marginRight: '10%' }}
                                label={item_qty_label}
                                onChangeText={(text) => {
                                    setQty({ value: text });
                                }}
                                value={qty.stringValue}
                            /> : null
                    }

                    <InputText
                        containerStyle={{ width: '60%', flexGrow: 1 }}
                        label={`${item_price_label} (${currency_symbol})`}
                        onChangeText={(text) => {
                            setPrice({ value: text });
                        }}
                        value={price.stringValue}
                    />

                </View>

                {
                    item_discount_inclusion ?
                        <View style={tailwind('flex flex-row justify-between')}>

                            <InputSelect
                                containerStyle={{ width: '30%' }}
                                label="Disc Type"
                                select={{
                                    selected: item_discount_type,
                                    options: [
                                        {
                                            label: 'Flat',
                                            value: 'flat'
                                        },
                                        {
                                            label: 'Percent',
                                            value: 'percent'
                                        },
                                    ]
                                }}
                                onSelectValueChange={(text) => updateStringValueToTempStash(text, 'item_discount_type')}
                            />

                            <InputText
                                containerStyle={{ width: '60%' }}
                                label={`${item_discount_label} ${item_discount_type === 'percent' ? '(%)' : `(${currency_symbol})`}`}
                                onChangeText={(text) => {
                                    setDiscountValue({ value: text });
                                }}
                                value={discountValue.stringValue}
                            />

                        </View> : null
                }



                <View style={tailwind('mb-4')}>
                    <TotalLabel label={item_amount_label} value={item_amount} />
                </View>

            </View>


            {formActionButton}


            <View style={tailwind('py-2')}>
                <ChargedItemsList />
            </View>


        </Card>

    )
}