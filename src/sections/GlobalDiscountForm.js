import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { InputText, InputSelect, OptionalFormCard, EditModeBadge } from '../components';
import useDecimalInput from '../utility/useDecimalInput';
import { View } from 'react-native';
import tailwind from 'tailwind-rn';
import {
    setValueOnCalcStash,
    putItemFromCalcStashToCalculation,
} from '../models/invoice/invoice-calculations.slice';
import useCurrency from './useCurrency';



export default function GlboalDiscountForm(props) {

    const { hideForm, hideToggleButton } = props;

    const dispatch = useDispatch()

    const global_discount_stash = useSelector(state => state.invoiceCalculations.calc_stash.global_invoice_discount);

    const [globalDiscountValue, setGlobalDiscountValue] = useDecimalInput(global_discount_stash.value)

    const setValueOnCalcStashHandler = (name, properties, value) => dispatch(setValueOnCalcStash({ name, properties, value }));

    const putItemFromCalcStashToCalculationHandler = (name) => dispatch(putItemFromCalcStashToCalculation({ name }));

    const [currencySymbol, setCurrencySymbol] = useCurrency();


    useEffect(() => {
        setValueOnCalcStashHandler('global_invoice_discount', 'value', globalDiscountValue.floatValue)
    }, [globalDiscountValue])

    return (
        <OptionalFormCard
            title={global_discount_stash.enabled ? 'Edit invoice discount' : 'Add invoice discount'}
            mainButtonText={global_discount_stash.enabled ? 'Save' : 'Add discount'}
            onCancel={() => {
                hideForm()
                if (global_discount_stash.enabled) {
                    hideToggleButton()
                }
            }}
            onSubmit={() => {
                putItemFromCalcStashToCalculationHandler('global_invoice_discount');
                hideToggleButton()
                hideForm()
            }}
        >

            {
                global_discount_stash.enabled ?
                    <EditModeBadge containerStyle={tailwind('mb-4')} /> : null
            }


            <View style={tailwind('flex flex-row justify-between')}>


                <InputSelect
                    containerStyle={{ width: '30%' }}
                    label="Disc Type"
                    select={{
                        selected: global_discount_stash.type,
                        options: [
                            {
                                label: 'Flat',
                                value: 'flat'
                            },
                            {
                                label: 'Percent',
                                value: 'percent'
                            },
                        ]
                    }}
                    onSelectValueChange={(text) => setValueOnCalcStashHandler('global_invoice_discount', 'type', text)}
                />

                <InputText
                    containerStyle={{ width: '60%' }}
                    label={`Discount (${global_discount_stash.type === 'percent' ? '%' : currencySymbol})`}
                    onChangeText={(text) => setGlobalDiscountValue({ value: text })}
                    value={globalDiscountValue.stringValue}
                />

            </View>


        </OptionalFormCard>

    )
}

