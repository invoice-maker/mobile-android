import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { InputText, InputSelect, OptionalFormCard, EditModeBadge } from '../components';
import useDecimalInput from '../utility/useDecimalInput';
import { View } from 'react-native';
import tailwind from 'tailwind-rn';
import {
    setValueOnCalcStash,
    putItemFromCalcStashToCalculation,
} from '../models/invoice/invoice-calculations.slice';
import useCurrency from './useCurrency';



export default function TaxForm(props) {

    const { hideForm, hideToggleButton } = props;

    const dispatch = useDispatch()

    const tax_stash = useSelector(state => state.invoiceCalculations.calc_stash.tax);

    const [taxValue, setTaxValue] = useDecimalInput(tax_stash.value)

    const setValueOnCalcStashHandler = (name, properties, value) => dispatch(setValueOnCalcStash({ name, properties, value }));

    const putItemFromCalcStashToCalculationHandler = (name) => dispatch(putItemFromCalcStashToCalculation({ name }));

    const [currencySymbol, setCurrencySymbol] = useCurrency();


    useEffect(() => {
        setValueOnCalcStashHandler('tax', 'value', taxValue.floatValue)
    }, [taxValue])

    return (
        <OptionalFormCard
            title={tax_stash.enabled ? 'Edit tax' : 'Add tax'}
            mainButtonText={tax_stash.enabled ? 'Save' : 'Add tax'}
            onCancel={() => {
                hideForm()
                if (tax_stash.enabled) {
                    hideToggleButton()
                }
            }}
            onSubmit={() => {
                putItemFromCalcStashToCalculationHandler('tax');
                hideToggleButton()
                hideForm()
            }}
        >

            {
                tax_stash.enabled ?
                    <EditModeBadge containerStyle={tailwind('mb-4')} /> : null
            }



            <View style={tailwind('flex flex-row justify-between')}>

                <InputSelect
                    containerStyle={{ width: '30%' }}
                    label="Tax Type"
                    select={{
                        selected: tax_stash.type,
                        options: [
                            {
                                label: 'Flat',
                                value: 'flat'
                            },
                            {
                                label: 'Percent',
                                value: 'percent'
                            },
                        ]
                    }}
                    onSelectValueChange={(text) => setValueOnCalcStashHandler('tax', 'type', text)}
                />

                <InputText
                    containerStyle={{ width: '60%' }}
                    label={`Tax (${tax_stash.type === 'percent' ? '%' : currencySymbol})`}
                    onChangeText={(text) => setTaxValue({ value: text })}
                    value={taxValue.stringValue}
                />

            </View>


        </OptionalFormCard>

    )
}