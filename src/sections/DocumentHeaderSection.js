import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Card, InputText, InputTextArea, BottomSheetSwitch } from '../components';
import { setStringValue } from '../models/invoice/invoice-references.slice';
import { setInclusionValue } from '../models/invoice/invoice-inclusions.slice';


function OptionsComponent() {

    const dispatch = useDispatch()

    const ship_to = useSelector(state => state.invoiceInclusions.ship_to);

    const onShipToChangeHandler = (value) => dispatch(setInclusionValue({ name: 'ship_to', value }));

    return (
        <>
            <BottomSheetSwitch
                title='Enable shipping address'
                subtitle='Include shipping address section on the invoice'
                onValueChange={onShipToChangeHandler}
                value={ship_to}
            />
        </>
    )
}



export default function DocumentReferenceSection() {

    const bill_to = useSelector(state => state.invoiceReferences.bill_to);
    const ship_to = useSelector(state => state.invoiceReferences.ship_to);
    const bill_to_label = useSelector(state => state.invoiceLabels.bill_to_label)
    const ship_to_label = useSelector(state => state.invoiceLabels.ship_to_label)

    const ship_to_inclusion = useSelector(state => state.invoiceInclusions.ship_to);

    const dispatch = useDispatch()

    const updateStringReferences = (text, name) => dispatch(setStringValue({
        name,
        value: text
    }))

    return (

        <Card
            title="Invoice header"
            divider
            withOption={true}
            optionsTitle='Invoice header Options'
            OptionsComponent={OptionsComponent}
        >
            <InputTextArea
                onChangeText={(text) => updateStringReferences(text, 'bill_to')}
                label={bill_to_label}
                value={bill_to}
            />

            {
                ship_to_inclusion ?
                    <InputTextArea
                        onChangeText={(text) => updateStringReferences(text, 'ship_to')}
                        label={ship_to_label}
                        value={ship_to}
                    /> : null

            }
        </Card>

    )
}