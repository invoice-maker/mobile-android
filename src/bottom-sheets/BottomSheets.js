import React, { useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Text, View } from 'react-native';
import { BottomSheet, ListItem } from 'react-native-elements';
import tailwind from 'tailwind-rn';

export default function BottomSheets() {

    const BottomSheets = createStackNavigator();

    const [isVisible, setIsVisible] = useState(false);
    const list = [
        { title: 'List Item 1' },
        { title: 'List Item 2' },
        {
            title: 'Cancel',
            containerStyle: { backgroundColor: 'red' },
            titleStyle: { color: 'white' },
            onPress: () => setIsVisible(false),
        },
    ];


    function Tes() {

        return (

            <View>
                <Text style={{ ...tailwind('h-full') }}>
                    something
                </Text>
            </View>
            // <BottomSheet
            //     isVisible={isVisible}
            //     containerStyle={{ backgroundColor: 'transparent' }}
            // >
            //     {list.map((l, i) => (
            //         <ListItem key={i} containerStyle={l.containerStyle} onPress={l.onPress}>
            //             <ListItem.Content>
            //                 <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
            //             </ListItem.Content>
            //         </ListItem>
            //     ))}
            // </BottomSheet>
        )


    }

    function Sheet() {
        return (
            <Text>Hellow</Text>
        )
    }


    function Shoot() {
        return (
            <Text>sbhoot</Text>
        )
    }

    const modalOptions = {
        headerShown: false,
        cardStyle: { backgroundColor: "transparent" },
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({ current: { progress } }) => ({
          cardStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 0.5, 0.9, 1],
              outputRange: [0, 0.1, 0.3, 0.7]
            })
          },
          overlayStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0.6],
              extrapolate: "clamp"
            })
          }
        })
      };

      
    return (
        <BottomSheets.Navigator
            mode="modal"
            headerMode="screen"
        >
            <BottomSheets.Screen name="Main" component={Shoot} options={{ cardStyle: { backgroundColor: "transparent" } }} />
            <BottomSheets.Screen name="sheet" component={Tes} options={modalOptions} />
        </BottomSheets.Navigator>
    )
}