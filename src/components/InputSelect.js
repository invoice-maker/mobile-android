import React from 'react';
import SelectField from './core/SelectField';
import InputWrapper from './core/InputWrapper';

export default function InputSelect(props) {

    return (
        <InputWrapper {...props} InputComponent={SelectField} />
    )
}
