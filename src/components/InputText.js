import React from 'react';
import InputWrapper from './core/InputWrapper';

export default function InputText(props) {

    return (
        <InputWrapper {...props} />
    )
}
