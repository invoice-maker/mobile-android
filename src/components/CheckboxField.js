import React from 'react';
import {View} from 'react-native';
import { CheckBox } from 'react-native-elements';
import tailwind from 'tailwind-rn';

export default function CheckboxField(props) {

    const { title, checked, onPress } = props
    // const [isChecked, setIsChecked] = useState(checked)

    return (

        <View style={tailwind('mb-4')}>
            <CheckBox
                textStyle={tailwind('text-base text-gray-400')}
                title={title}
                checked={checked}
                onPress={onPress}
            />

        </View>
    )
}