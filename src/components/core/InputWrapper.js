import React from 'react';
import { Input as InputRNE } from 'react-native-elements';
import tailwind from 'tailwind-rn';
import InputLabel from './InputLabel';

export default function InputWrapper(props) {

    const { label, containerStyle, value, onChangeText, tooltip, tooltipHeight, tooltipWidth, InputComponent } = props

    const inputRef = React.createRef();

    return (

        <InputRNE
            {...props}
            labelStyle={tailwind('mb-2')}
            containerStyle={{ ...tailwind('p-0'), ...containerStyle }}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            inputStyle={{ ...tailwind('bg-gray-100 rounded p-1 text-base') }}
            InputComponent={InputComponent}
            onChangeText={onChangeText}
            label={<InputLabel tooltip={tooltip} width={tooltipWidth} tooltipHeight={tooltipHeight} label={label} />}
            value={value}
            ref={inputRef}
        />

    )
}

