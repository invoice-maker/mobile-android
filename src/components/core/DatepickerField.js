import React, { useState } from 'react';
import { Input as InputRNE, Button, Icon } from 'react-native-elements';
import tailwind, { getColor } from 'tailwind-rn';
import DateTimePicker from '@react-native-community/datetimepicker';


const DatepickerField = React.forwardRef((props, ref) => {

    const { datepickerDisabled, hideDatepicker } = props
    const updateDateValue = props.onChange;
    const string_date = props.value.string_date;
    const unix_date = props.value.unix_date;

    const [showDatepicker, setShowDatepicker] = useState(false)

    if(hideDatepicker){
        return null
    }

    return (

        

        <>
            <Button
                disabled={datepickerDisabled}
                disabledTitleStyle={tailwind('text-gray-500')}
                titleStyle={{ flex: 1, flexGrow: 1, overflow: 'hidden' }}
                onPress={() => setShowDatepicker(true)}
                containerStyle={{ ...tailwind('flex flex-col w-full justify-between') }}
                iconContainerStyle={{ ...tailwind(''), flex: 1, flexGrow: 1 }}
                buttonStyle={{ ...tailwind('flex flex-row w-full justify-between'), height: 40 }}
                titleProps={{
                    numberOfLines: 1
                }}
                type='outline'
                iconRight={true}
                icon={
                    <Icon
                        name="calendar"
                        size={28}
                        type='ant-design'
                        color={datepickerDisabled ? getColor('gray-500') : getColor('blue-500')}
                    />
                }
                title={string_date || 'Pick a date'}
            />

            {showDatepicker && (
                <DateTimePicker
                    onChange={(e, selectedDate) => {
                        setShowDatepicker(false)
                        updateDateValue(selectedDate)
                    }}
                    value={unix_date|| new Date() } />
            )}

        </>
    )
})

export default DatepickerField;