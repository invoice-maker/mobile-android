import React from 'react';
import { Picker } from '@react-native-picker/picker';
import { View } from 'react-native';
import tailwind from 'tailwind-rn';

const SelectField = React.forwardRef((props, ref) => {

    const { onSelectValueChange } = props;
    const { selected, options } = props.select;


    return (
        <View style={tailwind('w-full bg-gray-100 rounded')}>
            <Picker
                selectedValue={selected}
                style={{ ...tailwind('w-full'), height: 40.25 }}
                onValueChange={(itemValue, itemIndex) => onSelectValueChange(itemValue)}
            >
                {
                    options.map(item => {
                        return <Picker.Item key={item.value} label={item.label} value={item.value} />

                    })
                }
            </Picker>
        </View>
    )
})

export default SelectField;
