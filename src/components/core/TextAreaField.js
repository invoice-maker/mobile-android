import React from 'react';
import { TextInput } from 'react-native';
import tailwind from 'tailwind-rn';

const TextAreaField = React.forwardRef((props, ref) => {

    const { value, onChangeText, numberOfLines } = props

    return (
        <TextInput
            ref={ref}
            value={value}
            onChangeText={onChangeText}
            style={{ ...tailwind('bg-gray-100 rounded w-full p-1 text-base'), textAlignVertical: 'top' }}
            multiline={true}
            numberOfLines={numberOfLines || 4}
        />
    )
})

export default TextAreaField;
