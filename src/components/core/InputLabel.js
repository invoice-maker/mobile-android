import React from 'react';
import { Icon, Tooltip } from 'react-native-elements'
import { Text, View } from 'react-native';
import tailwind, { getColor } from 'tailwind-rn';

const TipsIcon = React.forwardRef((props, ref) => {

    const { tooltip, tooltipHeight, height } = props

    return (
        <Tooltip height={height} popover={<Text style={tailwind('text-white')}>{tooltip}</Text>}>
            <Icon
                size={24}
                name='help-circle'
                type='material-community'
                color={getColor('yellow-400')}
            />
        </Tooltip >
    )
})


const InputLabel = React.forwardRef((props, ref) => {

    const { label, tooltip, tooltipHeight } = props

    return (
        <View style={tailwind('flex flex-row justify-between items-center mb-2')}>
            <Text style={{ ...tailwind('font-bold text-base'), color: '#90a4ae' }}>
                {label}
            </Text>
            {tooltip ? <TipsIcon tooltip={tooltip} height={tooltipHeight || 40} /> : null}
        </View>
    )
})

export default InputLabel;