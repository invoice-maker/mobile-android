import React from 'react';
import { View, Text } from 'react-native';
import tailwind from 'tailwind-rn';

export default function EditModeBadge(props) {

    const { containerStyle } = props

    return (
        <View style={containerStyle}>
            <View style={tailwind('flex flex-row items-center justify-center')}>
                <Text style={tailwind('py-1 px-2 bg-yellow-200 rounded-full text-base text-center')}>Edit mode</Text>
            </View>
        </View>
    )
}