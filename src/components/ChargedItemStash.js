import React, { useState } from 'react';
import { Icon, ListItem } from 'react-native-elements';
import { View, Text, Switch } from 'react-native';
import tailwind, { getColor } from 'tailwind-rn';
import BottomSheet from './BottomSheet';

export default function ChargedItemStash(props) {

    const {
        item_name,
        item_description,
        item_qty,
        item_price,
        item_discount_type,
        item_discount,
        item_amount,
        withOptions,
        optionsTitle,
        OptionsComponent,
    } = props

    const [isVisible, setIsVisible] = useState(false);

    let OptionIcon
    if (withOptions) {

        OptionIcon = (
            <View style={tailwind('absolute right-0')}>
                <Icon
                    color={getColor('blue-500')}
                    size={30}
                    name='dots-vertical'
                    type='material-community'
                    onPress={() => setIsVisible(true)}
                />
            </View>
        )

    }


    return (

        <>
            <View style={tailwind('p-2 bg-blue-100 rounded border border-blue-300')} onPress={() => { console.log('im touched') }} >
                <View style={{ ...tailwind('mb-2 flex flex-row'), }}>


                    <View style={{ flex: 1, flexGrow: 1 }}>
                        <Text style={tailwind('text-lg font-bold')}>
                            {item_name || '-'}
                        </Text>

                        {item_description ?
                            <Text style={tailwind('text-base')}>{item_description}</Text> : null}

                    </View>

                    {OptionIcon}

                </View>

                <View style={tailwind('flex flex-row flex-wrap')}>

                    <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                        <Text style={tailwind('font-bold text-base')}>
                            Qty
                </Text>
                        <Text style={tailwind('text-base')}>
                            {item_qty || '-'}
                        </Text>
                    </View>

                    <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                        <Text style={tailwind('font-bold text-base')}>
                            Price
                </Text>
                        <Text style={tailwind('text-base')}>
                            {item_price || '-'}
                        </Text>
                    </View>
                    <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                        <Text style={tailwind('font-bold text-base')}>
                            Discount
                </Text>
                        <Text style={tailwind('text-base')}>
                            {item_discount_type === 'flat' ? item_discount : null}
                            {item_discount_type === 'percent' ? `${item_discount || '-'}%` : null}
                        </Text>
                    </View>
                    <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                        <Text style={tailwind('font-bold text-base')}>
                            Amount
                    </Text>
                        <Text style={tailwind('text-base')}>
                            {item_amount}
                        </Text>
                    </View>

                </View>
            </View>


            <BottomSheet
                {...props}
                bottomSheetTitle={optionsTitle}
                isVisible={isVisible}
                hideBottomSheet={() => { setIsVisible(false) }}
                ChildrenComponent={OptionsComponent}
            >
             
            </BottomSheet>


        </>
    )
}