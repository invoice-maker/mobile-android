import React from 'react';
import TextAreaField from './core/TextAreaField';
import InputWrapper from './core/InputWrapper';


export default function InputTextArea(props) {

    return (
        <InputWrapper {...props} InputComponent={TextAreaField} />
    )
}


