import React, { useState } from 'react';
import { Text, View, Switch } from 'react-native';
import { Card as RNECard, Icon, ListItem } from 'react-native-elements';
import BottomSheet from '../components/BottomSheet';
import tailwind, { getColor } from 'tailwind-rn';


const Card = React.forwardRef((props, ref) => {
    const { title, divider, withOption, optionsTitle, OptionsComponent } = props

    const [isVisible, setIsVisible] = useState(false);

    let OptionIcon
    if (withOption) {
        OptionIcon = (
            <View style={tailwind('absolute right-0')}>
                <Icon
                    onPress={() => setIsVisible(true)}
                    size={25}
                    name='setting'
                    type='antdesign'
                    color={getColor('gray-400')}
                />
            </View>
        )
    }

    let Header
    if (title) {
        Header = (
            <View style={tailwind('pb-3')}>
                <Text style={tailwind('uppercase font-bold text-base text-center')}>{title}</Text>
                {OptionIcon}
            </View>
        )
    }

    return (
        <>
            <RNECard ref={ref} {...props}>

                {Header}

                {divider ? <RNECard.Divider /> : null}

                {props.children}

            </RNECard>

            <BottomSheet
                {...props}
                bottomSheetTitle={optionsTitle}
                isVisible={isVisible}
                hideBottomSheet={() => { setIsVisible(false) }}
                ChildrenComponent={OptionsComponent}
            >

            </BottomSheet>

            {/* <BottomSheet isVisible={isVisible} setVisibleToFalse={() => { setIsVisible(false) }}>

                <ListItem>
                    <View style={tailwind('flex flex-row justify-between')}>
                        <ListItem.Content>
                            <ListItem.Title>Enable payment terms</ListItem.Title>
                            <ListItem.Subtitle>
                                Include payment terms field on the invoice
                            </ListItem.Subtitle>
                        </ListItem.Content>

                        <View>
                            <Switch
                                trackColor={{ false: "#767577", true: "#81b0ff" }}
                                thumbColor={false ? "#f5dd4b" : "#f4f3f4"}
                                ios_backgroundColor="#3e3e3e"
                                value={false}
                            />
                        </View>

                    </View>

                </ListItem>

            </BottomSheet> */}
        </>

    )

});

export default Card;

// export default function Card(props) {

//     const { title, divider, withOption } = props

//     const [isVisible, setIsVisible] = useState(false);

//     let OptionIcon
//     if (withOption) {
//         OptionIcon = (
//             <View style={tailwind('absolute right-0')}>
//                 <Icon
//                     onPress={() => setIsVisible(true)}
//                     size={25}
//                     name='setting'
//                     type='antdesign'
//                     color={getColor('gray-400')}
//                 />
//             </View>
//         )
//     }

//     let Header
//     if (title) {
//         Header = (
//             <View style={tailwind('pb-3')}>
//                 <Text style={tailwind('uppercase font-bold text-base text-center')}>{title}</Text>
//                 {OptionIcon}
//             </View>
//         )
//     }

//     return (
//         <>
//             <RNECard>

//                 {Header}

//                 {divider ? <RNECard.Divider /> : null}

//                 {props.children}

//             </RNECard>

//             <BottomSheet isVisible={isVisible} setVisibleToFalse={() => { setIsVisible(false) }}>

//                 <ListItem>
//                     <View style={tailwind('flex flex-row justify-between')}>
//                         <ListItem.Content>
//                             <ListItem.Title>Enable payment terms</ListItem.Title>
//                             <ListItem.Subtitle>
//                                 Include payment terms field on the invoice
//                             </ListItem.Subtitle>
//                         </ListItem.Content>

//                         <View>
//                             <Switch
//                                 trackColor={{ false: "#767577", true: "#81b0ff" }}
//                                 thumbColor={false ? "#f5dd4b" : "#f4f3f4"}
//                                 ios_backgroundColor="#3e3e3e"
//                                 value={false}
//                             />
//                         </View>

//                     </View>

//                 </ListItem>

//             </BottomSheet>
//         </>

//     )

// }