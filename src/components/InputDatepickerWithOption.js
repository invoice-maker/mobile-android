import React from 'react';
import DatepickerField from './core/DatepickerField';
import InputWrapper from './core/InputWrapper';
import { View } from 'react-native';
import SelectField from './core/SelectField';
import tailwind from 'tailwind-rn';


const Picker = React.forwardRef((props, ref) => {

    return (
        <View style={tailwind('flex flex-row w-full overflow-hidden justify-between')}>
            <View style={{ ...tailwind('mr-4'), width: '37%' }}>
                <SelectField {...props} />
            </View>
            <View style={{ ...tailwind('flex'), flex: 1, flexGrow: 1 }}>
                <DatepickerField {...props} />
            </View>
        </View>
    )
})

export default function InputDatepickerWithOption(props) {

    return (
        <InputWrapper {...props} InputComponent={Picker} />
    )
}
