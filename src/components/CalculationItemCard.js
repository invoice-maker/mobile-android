import React, { useState } from 'react';
import { Text, View } from 'react-native';
import { Icon } from 'react-native-elements';
import tailwind, { getColor } from 'tailwind-rn';
import BottomSheet from './BottomSheet';

export default function CalculationItemCard(props) {

    const { label, value, withOptions, optionsTitle, OptionsComponent } = props

    const [isVisible, setIsVisible] = useState(false);

    const showBottomSheet = () => setIsVisible(true);

    const hideBottomSheet = () => setIsVisible(false);

    let OptionButton
    if (withOptions) {

        OptionButton = (
            <Icon
                color={getColor('blue-500')}
                size={30}
                name='dots-vertical'
                type='material-community'
                onPress={showBottomSheet}
            />
        )

    }


    return (

        <>
            <View style={{ ...tailwind('flex flex-row p-1 justify-between bg-blue-100 items-center border border-blue-300 rounded my-1') }}>
                <Text style={{ ...tailwind('font-bold text-base text-black'), width: '40%' }}>
                    {label}
                </Text>

                <View style={{ ...tailwind('flex flex-row items-center'), justifyContent: 'flex-end', flex: 1, flexGrow: 1 }}>
                    <Text style={tailwind('font-bold text-base text-black text-right')}>
                        {value}
                    </Text>

                    {OptionButton}

                </View>


            </View>

            <BottomSheet
                {...props}
                bottomSheetTitle={optionsTitle}
                isVisible={isVisible}
                hideBottomSheet={hideBottomSheet}
                ChildrenComponent={OptionsComponent}
            >

            </BottomSheet>


        </>
    )
}