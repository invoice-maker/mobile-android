import React from 'react';
import { View, Text } from 'react-native';
import tailwind from 'tailwind-rn';

export default function TotalLabel(props) {

    const { label, value } = props;

    return (
        <View style={tailwind('flex flex-row justify-between border-t border-b py-2 border-blue-300')}>
            <Text style={tailwind('font-bold text-lg text-black')}>
                {label}
            </Text>

            <Text style={tailwind('font-bold text-lg text-black')}>
                {value}
            </Text>
        </View>
    )
}