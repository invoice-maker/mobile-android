import React from 'react';
import { ListItem } from 'react-native-elements';
import { Switch, View, TouchableWithoutFeedback } from 'react-native';
import tailwind, { getColor } from 'tailwind-rn';

export default function BottomSheetSwitch(props) {

    const { title, subtitle, value, onValueChange } = props;

    const toggleSwitch = () => onValueChange(!value);

    return (
        <TouchableWithoutFeedback onPress={toggleSwitch}>
            <ListItem>
                <View style={tailwind('flex flex-row justify-between')}>
                    <ListItem.Content>
                        <ListItem.Title>{title}</ListItem.Title>
                        <ListItem.Subtitle>
                            {subtitle}
                        </ListItem.Subtitle>
                    </ListItem.Content>

                    <View>
                        <Switch
                            trackColor={{ false: "#767577", true: getColor('blue-300') }}
                            thumbColor={value ? getColor('blue-500') : getColor('gray-200')}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={toggleSwitch}
                            value={value}
                        />
                    </View>

                </View>

            </ListItem>
        </TouchableWithoutFeedback>
    )

}