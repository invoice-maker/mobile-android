import React from 'react';
import DatepickerField from './core/DatepickerField';
import InputWrapper from './core/InputWrapper';

export default function InputDatepicker(props) {

    return (
        <InputWrapper {...props} InputComponent={DatepickerField} />
    )
}
