import React from 'react';
import { View, Text } from 'react-native';
import tailwind from 'tailwind-rn';
import CardDivider from './CardDivider';
import { Button } from 'react-native-elements';

export default function OptionalFormCard(props) {

    const { children, title, mainButtonText, onCancel, onSubmit } = props;

    return (

        < View style={tailwind('my-4 bg-white border border-gray-300 p-2 rounded')} >

            <Text style={tailwind('mb-2 text-base font-bold text-black text-center')}>{title}</Text>

            <CardDivider />

            {children}

            <View style={tailwind('flex flex-row justify-between')}>
                <Button
                    onPress={onCancel}
                    containerStyle={{ width: '45%' }}
                    buttonStyle={{ borderColor: 'red' }}
                    titleStyle={{ color: 'red' }}
                    title="Cancel"
                    type="outline"
                />

                <Button
                    containerStyle={{ width: '45%' }}
                    title={mainButtonText}
                    type="outline"
                    onPress={onSubmit}
                />
            </View>

        </View >

    )
}
