import React, { useContext } from 'react';
import { TouchableWithoutFeedback, View, Text } from 'react-native';
import { BottomSheet as RNEBotttomSheet, Icon, Divider } from 'react-native-elements';
import HeightContext from '../context/HeightContext';
import tailwind from 'tailwind-rn';

export default function BottomSheet(props) {

    const { isVisible, hideBottomSheet, children, bottomSheetTitle, ChildrenComponent } = props

    const height = useContext(HeightContext)


    return (

        <RNEBotttomSheet isVisible={isVisible} modalProps={{ onRequestClose: () => hideBottomSheet() }}>
            <TouchableWithoutFeedback onPress={hideBottomSheet}>
                <View style={{ flex: 1, height: height }}   >
                    <TouchableWithoutFeedback>
                        <View style={tailwind('absolute bottom-0 right-0 left-0 bg-white flex p-5 mx-1 rounded-t-2xl')}>

                            <View style={tailwind('pt-1 pb-4')}>
                                <Text style={tailwind('uppercase font-bold text-base text-center')}>{bottomSheetTitle || 'Options'}</Text>
                                <Icon
                                    onPress={hideBottomSheet}
                                    containerStyle={tailwind('absolute right-0 top-1/4')}
                                    size={25}
                                    name='window-close'
                                    type='material-community'
                                />
                            </View>

                            <Divider style={{ height: 1 }} />

                            <View>

                                {
                                    ChildrenComponent ?
                                        <ChildrenComponent {...props} />
                                        : null
                                }

                            </View>



                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </TouchableWithoutFeedback>
        </RNEBotttomSheet >


    )
}