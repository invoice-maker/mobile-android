import Constants from 'expo-constants';

let serverUrl

if (__DEV__) {
    if (Constants.isDevice) {
        serverUrl = 'http://192.168.100.6:5005/'
    } else {
        serverUrl = 'http://10.0.2.2:5005/'
    }
} else {
    serverUrl = 'https://vivainvoice-backend-api.web.app/'
}

export { serverUrl };


