import React from 'react';
import { ScrollView, View, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import tailwind from 'tailwind-rn';
import { Card, InputText, InputSelect, CheckboxField } from '../../../components';
import locale from '../../../data/locale';
import currency from '../../../data/currency';
import { setStringValue as SetInvoiceReferenceStringValue } from '../../../models/invoice/invoice-references.slice';
import { setNumberFormat, setCurrencyCode, setCurrencySymbolOnLeft, setCustomCurrencySymbol } from '../../../models/invoice/invoice-calculations.slice';
import { setPaperSize } from '../../../models/invoice/invoice-export-options.slice';

export default function InvoiceFormats() {

    const date_format = useSelector(state => state.invoiceReferences.date_format)
    const date_local = useSelector(state => state.invoiceReferences.date_local)
    const number_format = useSelector(state => state.invoiceCalculations.number_format)
    const currency_code = useSelector(state => state.invoiceCalculations.currency_code)
    const currency_on_left = useSelector(state => state.invoiceCalculations.currency_symbol_on_left)
    const custom_currency = useSelector(state => state.invoiceCalculations.currency_symbol_on_left.currency_symbol)
    const paper_size = useSelector(state => state.invoiceExportOptions.paper_size)

    const dispatch = useDispatch()

    const setReferencesStringValueHandler = (value, name) => dispatch(SetInvoiceReferenceStringValue({ name, value }))

    const setNumberFormatHandler = (value) => dispatch(setNumberFormat({ value }))

    const seCurrencyCodeHandler = (value) => dispatch(setCurrencyCode({ value }))

    const setCurrencySymbolOnLeftHandler = () => dispatch(setCurrencySymbolOnLeft({ value: !currency_on_left }))

    const setCustomCurrencySymbolHandler = (value) => dispatch(setCustomCurrencySymbol({ value }))

    const setPaperSizeHandler = (value) => dispatch(setPaperSize({ value }))


    return (
        <ScrollView>

            <Card title="Date Format" divider>


                <InputSelect
                    tooltip='Select date format for invoice date and due date'
                    tooltipHeight={80}
                    label="Date Format"
                    select={{
                        selected: date_format,
                        options: [
                            { label: 'DD-MMM-YYYY (21-MAR-2020)', value: 'DD-MMM-YYYY' },
                            { label: 'DD/MMM/YYYY (21/MAR/2020)', value: 'DD/MMM/YYYY' },
                            { label: 'DD MMM YYYY (21 MAR 2020)', value: 'DD MMM YYYY' },
                            { label: 'DD-MM-YYYY (21-03-2020)', value: 'DD-MM-YYYY' },
                            { label: 'DD/MM/YYYY (21/03/2020)', value: 'DD/MM/YYYY' },
                            { label: 'DD MM YYYY (21 03 2020)', value: 'DD MM YYYY' },
                            { label: 'DD-MMMM-YYYY (21-March-2020)', value: 'DD-MMMM-YYYY' },
                            { label: 'DD/MMMM/YYYY (21/March/2020)', value: 'DD/MMMM/YYYY' },
                            { label: 'DD MMMM YYYY (21 March 2020)', value: 'DD MMMM YYYY' },
                            { label: 'MMM-DD-YYYY (MAR-21-2020)', value: 'MMM-DD-YYYY' },
                            { label: 'MMM/DD/YYYY (MAR/21/2020)', value: 'MMM/DD/YYYY' },
                            { label: 'MMM DD YYYY (MAR 21 2020)', value: 'MMM DD YYYY' },
                            { label: 'MM-DD-YYYY (03-21-2020)', value: 'MM-DD-YYYY' },
                            { label: 'MM/DD/YYYY (03/21/2020)', value: 'MM/DD/YYYY' },
                            { label: 'MM DD YYYY (03 21 2020)', value: 'MM DD YYYY' },
                            { label: 'MMMM-DD-YYYY (March-21-2020)', value: 'MMMM-DD-YYYY' },
                            { label: 'MMMM/DD/YYYY (March/21/2020)', value: 'MMMM/DD/YYYY' },
                            { label: 'MMMM DD YYYY (March 21 2020)', value: 'MMMM DD YYYY' },
                            { label: 'YYYY-MMM-DD (2020-MAR-21)', value: 'YYYY-MMM-DD' },
                            { label: 'YYYY/MMM/DD (2020/MAR/21)', value: 'YYYY/MMM/DD' },
                            { label: 'YYYY MMM DD (2020 MAR 21)', value: 'YYYY MMM DD' },
                            { label: 'YYYY-MM-DD (2020-03-21)', value: 'YYYY-MM-DD' },
                            { label: 'YYYY/MM/DD (2020/03/21)', value: 'YYYY/MM/DD' },
                            { label: 'YYYY MM DD (2020 03 21)', value: 'YYYY MM DD' },
                            { label: 'YYYY-MMMM-DD (2020-March-21)', value: 'YYYY-MMMM-DD' },
                            { label: 'YYYY/MMMM/DD (2020/March/21)', value: 'YYYY/MMMM/DD' },
                            { label: 'YYYY MMMM DD (2020 March 21)', value: 'YYYY MMMM DD' },
                        ]
                    }}
                    onSelectValueChange={(text) => setReferencesStringValueHandler(text, 'date_format')}
                />

                <InputSelect
                    tooltip='Translate date to specific locale'
                    label="Date language"
                    select={{
                        selected: date_local,
                        options: locale
                    }}
                    onSelectValueChange={(text) => setReferencesStringValueHandler(text, 'date_local')}
                />

                <View style={tailwind('bg-yellow-200 border border-yellow-400 rounded p-1 mb-4')}>
                    <Text style={tailwind('text-base')}>
                        Date translation will be applied when the PDF is being generated
                    </Text>
                </View>

            </Card>



            <Card title="Currency Format" divider>

                <InputSelect
                    label="Select currency"
                    select={{
                        selected: currency_code,
                        options: currency
                    }}
                    onSelectValueChange={seCurrencyCodeHandler}
                />


                <InputSelect
                    tooltip={'Select number format based on local, example: 1 000,751 -or- 1,000.751 -or- 1.000,751 -or-  etc'}
                    tooltipHeight={120}
                    label="Number format"
                    select={{
                        selected: number_format,
                        options: locale
                    }}
                    onSelectValueChange={setNumberFormatHandler}
                />


                <CheckboxField
                    onPress={setCurrencySymbolOnLeftHandler}
                    checked={currency_on_left}
                    title="Put currency symbol on left"
                />


                <InputText
                    tooltip='If currency symbol of your choice is not listed, you can write your custom currency symbol here.'
                    tooltipHeight={110}
                    label="Custom currency symbol"
                    value={custom_currency}
                    onChangeText={setCustomCurrencySymbolHandler}
                />


                <View style={tailwind('bg-yellow-200 border border-yellow-400 rounded p-1 mb-4')}>
                    <Text style={tailwind('text-base')}>
                        Number format will be applied when the PDF being generated
                    </Text>
                </View>



            </Card>

            <Card title="Paper Format" divider>
                <InputSelect
                    tooltip={'Select number format based on local, example: 1 000,751 -or- 1,000.751 -or- 1.000,751 -or-  etc'}
                    tooltipHeight={120}
                    label="Paper size"
                    select={{
                        selected: paper_size,
                        options: [{ label: 'Letter', value: 'Letter' }, { label: 'Legal', value: 'Legal' }, { label: 'Tabloid', value: 'Tabloid' }, { label: 'Ledger', value: 'Ledger' }, { label: 'A0', value: 'A0' }, { label: 'A1', value: 'A1' }, { label: 'A2', value: 'A2' }, { label: 'A3', value: 'A3' }, { label: 'A4', value: 'A4' }, { label: 'A5', value: 'A5' }, { label: 'A6', value: 'A6' }]
                    }}
                    onSelectValueChange={setPaperSizeHandler}
                />
            </Card>


        </ScrollView>
    )
}
