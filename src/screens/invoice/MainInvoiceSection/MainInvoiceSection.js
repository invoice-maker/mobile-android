import React, { useRef } from 'react';
import { ScrollView } from 'react-native';
import DocumentReferenceSection from '../../../sections/DocumentReferenceSection';
import DocumentHeaderSection from '../../../sections/DocumentHeaderSection';
import CalculationSection from '../../../sections/CalculationSection';
import ChargedItemsSection from '../../../sections/ChargedItemsSection';
import DownloadBotton from '../../../sections/DownloadBotton';

export default function MainInvoiceSections(props) {

    const parentScrollViewRef = useRef(null)

    return (
        <ScrollView ref={parentScrollViewRef} style={{ flex: 1 }}>

            <DocumentReferenceSection />

            <DocumentHeaderSection />

            <ChargedItemsSection parentScrollViewRef={parentScrollViewRef} />

            <CalculationSection parentScrollViewRef={parentScrollViewRef} />

            <DownloadBotton />

        </ScrollView>
    )
}

