import React from 'react';
import { ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Card, InputText, InputTextArea, BottomSheetSwitch } from '../../../../src/components';
import { setLabel } from '../../../models/invoice/invoice-labels.slice';
import { setInclusionValue } from '../../../models/invoice/invoice-inclusions.slice';


function CustomMessageOption() {

    const dispatch = useDispatch()


    const greeting_message = useSelector(state => state.invoiceInclusions.greeting_message);

    const closing_message = useSelector(state => state.invoiceInclusions.closing_message);

    const onGreetingMessageChangeHandler = (value) => dispatch(setInclusionValue({ name: 'greeting_message', value }));

    const onClosingMessageChangeHandler = (value) => dispatch(setInclusionValue({ name: 'closing_message', value }));

    return (

        <>
            <BottomSheetSwitch
                title='Enable greeting message'
                subtitle='Include greeting message on the invoice'
                onValueChange={onGreetingMessageChangeHandler}
                value={greeting_message}
            />

            <BottomSheetSwitch
                title='Enable closing message'
                subtitle='Include closing message on the invoice'
                onValueChange={onClosingMessageChangeHandler}
                value={closing_message}
            />

        </>

    )

}

function InvoiceInfoOptions() {

    const dispatch = useDispatch()


    const notes = useSelector(state => state.invoiceInclusions.notes);

    const terms_and_conditions = useSelector(state => state.invoiceInclusions.terms_and_conditions);

    const footnote = useSelector(state => state.invoiceInclusions.footnote);



    const onNotesChangeHandler = (value) => dispatch(setInclusionValue({ name: 'notes', value }));

    const onTermsAndConditionsChangeHandler = (value) => dispatch(setInclusionValue({ name: 'terms_and_conditions', value }));

    const onFootnoteChangeHandler = (value) => dispatch(setInclusionValue({ name: 'footnote', value }));




    return (
        <>
            <BottomSheetSwitch
                title='Enable invoice notes'
                subtitle='Include notes section on the invoice'
                onValueChange={onNotesChangeHandler}
                value={notes}
            />
            <BottomSheetSwitch
                title='Enable terms and conditions'
                subtitle='Include terms and conditions on the invoice'
                onValueChange={onTermsAndConditionsChangeHandler}
                value={terms_and_conditions}
            />
            <BottomSheetSwitch
                title='Enable footnote'
                subtitle='Include footnote on the invoice'
                onValueChange={onFootnoteChangeHandler}
                value={footnote}
            />

        </>
    )
}



export default function InvoiceInfo() {

    const labels = useSelector(state => state.invoiceLabels)

    const notes_inclusion = useSelector(state => state.invoiceInclusions.notes);

    const terms_and_conditions_inclusion = useSelector(state => state.invoiceInclusions.terms_and_conditions);

    const footnote_inclusion = useSelector(state => state.invoiceInclusions.footnote);

    const greeting_message_inclusion = useSelector(state => state.invoiceInclusions.greeting_message);

    const closing_message_inclusion = useSelector(state => state.invoiceInclusions.closing_message);

    const dispatch = useDispatch()

    function changeTextLabelValue(labelName, value) {
        dispatch(setLabel({
            labelName,
            value
        }))
    }


    return (
        <ScrollView>

            <Card
                title="Company identity"
                divider
            >

                <InputText
                    label="Your business name"
                    value={labels.business_name_label}
                    onChangeText={(text) => { changeTextLabelValue('business_name_label', text) }}
                />

            </Card>



            <Card title="Invoice Info"
                divider
                withOption={true}
                optionsTitle='Charged Items Options'
                OptionsComponent={InvoiceInfoOptions}
            >

                <InputTextArea
                    label="Invoice issuer"
                    value={labels.invoice_issuer_information_content}
                    onChangeText={(text) => { changeTextLabelValue('invoice_issuer_information_content', text) }}
                    tooltip={'Your business name \nYour email address \nYour street address \nYour phone number \nYour city, state, zip'}
                    tooltipHeight={110}
                />


                {notes_inclusion ?
                    <InputTextArea
                        label={labels.notes_label}
                        value={labels.notes_content}
                        onChangeText={(text) => { changeTextLabelValue('notes_content', text) }}
                        tooltip={'Additional info for your customer like bank numbers, your tax number, etc'}
                        tooltipHeight={110}
                    /> : null
                }

                {terms_and_conditions_inclusion ?
                    <InputTextArea
                        label={labels.terms_and_conditions_label}
                        value={labels.terms_and_conditions_content}
                        onChangeText={(text) => { changeTextLabelValue('terms_and_conditions_content', text) }}
                        tooltip={'Any terms like terms of payment, return policy, revisions policy, etc'}
                        tooltipHeight={110}
                    /> : null
                }

                {footnote_inclusion ?
                    <InputTextArea
                        label="Invoice footnote"
                        value={labels.footnote_content}
                        onChangeText={(text) => { changeTextLabelValue('footnote_content', text) }}
                    /> : null
                }

            </Card>


            <Card title="Custom Message"
                divider
                withOption={true}
                optionsTitle='Custom Message Options'
                OptionsComponent={CustomMessageOption}
            >

                {greeting_message_inclusion ?
                    <InputTextArea
                        label="Greeting message"
                        value={labels.greeting_message_content}
                        onChangeText={(text) => { changeTextLabelValue('greeting_message_content', text) }}
                        tooltip={'Give greeting message to your customer or put any usefull information you needs here'}
                        tooltipHeight={110}
                    /> : null
                }

                {closing_message_inclusion ?
                    <InputTextArea
                        label="Closing message"
                        value={labels.closing_message_content}
                        onChangeText={(text) => { changeTextLabelValue('closing_message_content', text) }}
                        tooltip={'Give closing message to your customer or put any usefull information you needs here'}
                        tooltipHeight={110}
                    /> : null
                }

            </Card>



        </ScrollView>
    )
}
