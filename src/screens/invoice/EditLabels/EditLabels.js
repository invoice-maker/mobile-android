import React from 'react';
import { ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Card, InputText, CardDivider } from '../../../../src/components';
import { setLabel } from '../../../models/invoice/invoice-labels.slice';

export default function EditLabels() {

    const labels = useSelector(state => state.invoiceLabels)

    const dispatch = useDispatch()

    function changeTextLabelValue(labelName, value) {
        dispatch(setLabel({
            labelName,
            value
        }))
    }

    return (

        <ScrollView>

            <Card title="Invoice Reference Label" divider>
                <InputText
                    label="Invoice number label"
                    value={labels.invoice_number_label}
                    onChangeText={(text) => { changeTextLabelValue('invoice_number_label', text) }}
                />


                <InputText
                    label="Invoice date label"
                    value={labels.invoice_date_label}
                    onChangeText={(text) => { changeTextLabelValue('invoice_date_label', text) }}
                />

                <InputText
                    label="Payment terms label"
                    value={labels.payment_terms_label}
                    onChangeText={(text) => { changeTextLabelValue('payment_terms_label', text) }}
                />

                <InputText
                    label="Due date label"
                    value={labels.invoice_due_date_label}
                    onChangeText={(text) => { changeTextLabelValue('invoice_due_date_label', text) }}
                />

            </Card>


            <Card title="Invoice Header Label" divider>

                <InputText
                    label="Document name"
                    value={labels.document_name_label}
                    onChangeText={(text) => { changeTextLabelValue('document_name_label', text) }}
                />

                <InputText
                    label="Bill to label"
                    value={labels.bill_to_label}
                    onChangeText={(text) => { changeTextLabelValue('bill_to_label', text) }}
                />

                <InputText
                    label="Ship to label"
                    value={labels.ship_to_label}
                    onChangeText={(text) => { changeTextLabelValue('ship_to_label', text) }}
                />

            </Card>


            <Card title="Charged Items Label" divider>
                <InputText
                    label="Item name label"
                    value={labels.item_description_label}
                    onChangeText={(text) => { changeTextLabelValue('item_description_label', text) }}
                />

                <InputText
                    label="Item quantity label"
                    value={labels.item_qty_label}
                    onChangeText={(text) => { changeTextLabelValue('item_qty_label', text) }}
                />

                <InputText
                    label="Item discount label"
                    value={labels.item_discount_label}
                    onChangeText={(text) => { changeTextLabelValue('item_discount_label', text) }}
                />

                <InputText
                    label="Item price label"
                    value={labels.item_price_label}
                    onChangeText={(text) => { changeTextLabelValue('item_price_label', text) }}
                />

                <InputText
                    label="Item amount label"
                    value={labels.item_amount_label}
                    onChangeText={(text) => { changeTextLabelValue('item_amount_label', text) }}
                />

            </Card>


            <Card title="Invoice Calculation labels" divider>
                <InputText
                    label="Invoice subtotal label"
                    value={labels.subtotal_label}
                    onChangeText={(text) => { changeTextLabelValue('subtotal_label', text) }}
                />

                <InputText
                    label="Global discount label"
                    value={labels.global_discount_label}
                    onChangeText={(text) => { changeTextLabelValue('global_discount_label', text) }}
                />

                <InputText
                    label="Tax label"
                    value={labels.tax_label}
                    onChangeText={(text) => { changeTextLabelValue('tax_label', text) }}
                />

                <InputText
                    label="Shipping cost label"
                    value={labels.shipping_label}
                    onChangeText={(text) => { changeTextLabelValue('shipping_label', text) }}
                />


                <InputText
                    label="Balance due label"
                    value={labels.balance_due_label}
                    onChangeText={(text) => { changeTextLabelValue('balance_due_label', text) }}

                />

            </Card>

            <Card title="Invoice Information Labels" divider>

                <InputText
                    label="Invoice notes label"
                    value={labels.notes_label}
                    onChangeText={(text) => { changeTextLabelValue('notes_label', text) }}
                />

                <InputText
                    label="Invoice terms label"
                    value={labels.terms_and_conditions_label}
                    onChangeText={(text) => { changeTextLabelValue('terms_and_conditions_label', text) }}

                />

            </Card>

        </ScrollView>

    )
}