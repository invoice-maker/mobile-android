import React from 'react';
import { ScrollView, Text, View, Switch, TouchableHighlight } from 'react-native';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import tailwind, { getColor } from 'tailwind-rn';
import { Card, InputText, CardDivider } from '../components';
import { Button } from 'react-native-elements';
import { Icon } from 'react-native-elements';
import { BottomSheet, ButtonGroup } from 'react-native-elements';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

export default function CreateInvoice() {

    return (
        <ScrollView style={{ flex: 1 }}>


            <Card title="Invoice Reference" divider>
                <InputText
                    label="Invoice Number"
                />

                <InputText
                    label="Invoice Date"
                />

                <InputText
                    label="payment Terms"
                />

                <InputText
                    label="Due Date"
                />

            </Card>


            <Card title="Invoice header" divider>
                <InputText
                    label="Bill To"
                />

                <InputText
                    label="Ship To"
                />

            </Card>

            <Card title="Charged Items" divider>

                <View style={tailwind('rounded p-1')}>
                    <InputText
                        label="Item Name"
                    />
                    <View style={tailwind('flex flex-row justify-between')}>

                        <InputText
                            containerStyle={{ width: '30%' }}
                            label="Qty"
                        />

                        <InputText
                            containerStyle={{ width: '60%' }}
                            label="Price"
                        />

                    </View>

                    {/* <View style={tailwind('flex flex-row justify-between')}>

                <InputText
                  containerStyle={{ width: '30%' }}
                  label="Disc Type"
                />

                <InputText
                  containerStyle={{ width: '60%' }}
                  label="Dicount amount"
                />

              </View> */}
                    <View style={tailwind('mb-4')}>
                        <TouchableHighlight>
                            <View style={tailwind('py-2 flex flex-row')}>
                                <Icon
                                    color={getColor('gray-500')}
                                    name='chevron-right' />


                                <Text style={tailwind('text-base font-bold text-gray-500')}>
                                    More options
                    </Text>
                            </View>
                        </TouchableHighlight>
                        <View style={tailwind('p-1 bg-yellow-100 rounded')}>
                            <Text style={tailwind('text-base')}>
                                asdasda asdasdaasdadasdadasdasdadasdasda
                                asdad
                                asdad
                                asda
                                asdad
                                asda
                  </Text>
                        </View>
                    </View>

                    <View style={tailwind('flex flex-row items-center mb-2')}>
                        <Switch
                            trackColor={{ false: "#767577", true: "#81b0ff" }}
                            thumbColor={false ? "#f5dd4b" : "#f4f3f4"}
                            ios_backgroundColor="#3e3e3e"
                            // onValueChange={toggleSwitch}
                            value={false}
                        />



                        <View>


                            <Text style={tailwind('text-lg text-gray-700')}>
                                Enable discount per item basis
                  </Text>

                        </View>
                    </View>

                </View>


                <Button
                    title="Add item"
                    type="outline"
                />


                <CardDivider />

                <View style={tailwind('mb-4 p-2 bg-blue-100 rounded border border-blue-300')} onPress={() => { console.log('im touched') }} >
                    <View style={{ ...tailwind('mb-2 flex flex-row'), }}>

                        <View style={{ flex: 1, flexGrow: 1 }}>
                            <Text style={tailwind('text-lg font-bold')}>
                                Software development service
                  </Text>
                            <Text style={tailwind('text-base')}>
                                Xiaomi Mi 4i lorem asdad asda asda asda asdad asdad adsa asdad
                  </Text>
                        </View>

                        <View>

                            <Icon
                                color={getColor('blue-500')}
                                size={30}
                                name='dots-vertical'
                                type='material-community'
                            />

                        </View>

                    </View>

                    <View style={tailwind('flex flex-row flex-wrap')}>

                        <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                            <Text style={tailwind('font-bold text-base')}>
                                Qty
                  </Text>
                            <Text style={tailwind('text-base')}>
                                2
                  </Text>
                        </View>
                        <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                            <Text style={tailwind('font-bold text-base')}>
                                Price
                  </Text>
                            <Text style={tailwind('text-base')}>
                                Rp 15.000
                  </Text>
                        </View>
                        <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                            <Text style={tailwind('font-bold text-base')}>
                                Discount
                  </Text>
                            <Text style={tailwind('text-base')}>
                                10%
                  </Text>
                        </View>
                        <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                            <Text style={tailwind('font-bold text-base')}>
                                Amount
                  </Text>
                            <Text style={tailwind('text-base')}>
                                Rp 25.000
                  </Text>
                        </View>

                    </View>
                </View>


                <View style={tailwind('p-2 bg-blue-100 rounded border border-blue-300')} onPress={() => { console.log('im touched') }} >
                    <View style={{ ...tailwind('mb-2 flex flex-row'), }}>

                        <View style={{ flex: 1, flexGrow: 1 }}>
                            <Text style={tailwind('text-lg font-bold')}>
                                Software development service
                              </Text>
                            <Text style={tailwind('text-base')}>
                                Xiaomi Mi 4i lorem asdad asda asda asda asdad asdad adsa asdad
                            </Text>
                        </View>

                        <View>

                            <Icon
                                color={getColor('blue-500')}
                                size={30}
                                name='dots-vertical'
                                type='material-community'
                            />

                        </View>

                    </View>

                    <View style={tailwind('flex flex-row flex-wrap')}>

                        <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                            <Text style={tailwind('font-bold text-base')}>
                                Qty
                  </Text>
                            <Text style={tailwind('text-base')}>
                                2
                  </Text>
                        </View>
                        <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                            <Text style={tailwind('font-bold text-base')}>
                                Price
                  </Text>
                            <Text style={tailwind('text-base')}>
                                Rp 15.000
                  </Text>
                        </View>
                        <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                            <Text style={tailwind('font-bold text-base')}>
                                Discount
                  </Text>
                            <Text style={tailwind('text-base')}>
                                10%
                  </Text>
                        </View>
                        <View style={{ ...tailwind('text-base w-1/2 flex flex-col') }}>
                            <Text style={tailwind('font-bold text-base')}>
                                Amount
                  </Text>
                            <Text style={tailwind('text-base')}>
                                Rp 25.000
                  </Text>
                        </View>

                    </View>
                </View>


            </Card>


            <Card title='Invoice Calculation' divider>

                <View>

                    <View style={tailwind('flex flex-row py-1 justify-between')}>
                        <Text style={tailwind('w-1/2 font-bold text-base')}>
                            Invoice Subtotal
                </Text>

                        <Text style={tailwind('font-bold text-base')}>
                            Rp 250.000
                </Text>

                    </View>

                    <View style={{ ...tailwind('flex flex-row p-1 justify-between bg-blue-100 items-center border border-blue-300 rounded my-1') }}>
                        <Text style={{ ...tailwind('font-bold text-base text-black'), width: '40%' }}>
                            VAT (10%)
                </Text>

                        <View style={{ ...tailwind('flex flex-row items-center'), justifyContent: 'flex-end', flex: 1, flexGrow: 1 }}>
                            <Text style={tailwind('font-bold text-base text-black text-right')}>
                                Rp 15.000.000.000
                  </Text>

                            <Icon
                                color={getColor('blue-500')}
                                size={30}
                                name='dots-vertical'
                                type='material-community'
                            />
                        </View>


                    </View>


                    <View style={{ ...tailwind('flex flex-row p-1 justify-between bg-blue-100 items-center border border-blue-300 rounded my-1') }}>
                        <Text style={{ ...tailwind('font-bold text-base text-black'), width: '40%' }}>
                            VAT (10%)
                </Text>

                        <View style={{ ...tailwind('flex flex-row items-center'), justifyContent: 'flex-end', flex: 1, flexGrow: 1 }}>
                            <Text style={tailwind('font-bold text-base text-black text-right')}>
                                Rp 15.000.000.000
                  </Text>

                            <Icon
                                color={getColor('blue-500')}
                                size={30}
                                name='dots-vertical'
                                type='material-community'
                            />
                        </View>


                    </View>



                    <View style={tailwind('flex flex-row mt-4 mb-4 justify-between border-t border-b py-2 border-blue-300')}>
                        <Text style={tailwind('w-1/2 font-bold text-lg text-black')}>
                            Balance Due
                </Text>

                        <Text style={tailwind('font-bold text-lg text-black')}>
                            Rp 250.000
                </Text>

                    </View>

                    <ButtonGroup
                        // onPress={this.updateIndex}
                        containerStyle={tailwind('m-0')}
                        textStyle={tailwind('text-base')}
                        selectedIndex={0}
                        selectedButtonStyle={tailwind('bg-blue-500')}
                        buttons={['+ Discount', '+ Tax', '+ Shipping']}
                    // containerStyle={{ height: 100 }}
                    />
                    <View>


                        <View style={tailwind('flex flex-row justify-between')}>




                            {/* <Button
                    containerStyle={{ width: '33%' }}
                    title="+ Discount"
                    type="outline"
                  />
                  <Button
                    containerStyle={{ width: '33%' }}
                    title="+ Tax"
                    type="outline"
                  />

                  <Button
                    containerStyle={{ width: '33%' }}
                    title="+ Shipping"
                    type="outline"
                  /> */}
                        </View>

                    </View>


                    <View style={tailwind('my-4 bg-white border border-gray-300 p-2 rounded')}>

                        <View style={tailwind('flex flex-row justify-between')}>

                            <InputText
                                containerStyle={{ width: '30%' }}
                                label="Type"
                            />

                            <InputText
                                containerStyle={{ width: '60%' }}
                                label="Discount"
                            />



                        </View>

                        <View style={tailwind('flex flex-row justify-between')}>
                            <Button
                                containerStyle={{ width: '45%' }}
                                buttonStyle={{ borderColor: 'red' }}
                                titleStyle={{ color: 'red' }}
                                title="Cancel"
                                type="outline"
                            />

                            <Button
                                containerStyle={{ width: '45%' }}
                                title="Add Discount"
                                type="outline"
                            />
                        </View>


                    </View>


                </View>

            </Card>



        </ScrollView>
    )
}

