import { configureStore } from '@reduxjs/toolkit';
import invoiceLabelsReducer from './invoice/invoice-labels.slice';
import invoiceReferencesReducer from './invoice/invoice-references.slice';
import invoiceCalculationsReducer  from './invoice/invoice-calculations.slice';
import invoiceExportOptionsSliceReducer from './invoice/invoice-export-options.slice';
import invoiceInclusionsSliceReducer from './invoice/invoice-inclusions.slice';
import invoiceGeneratorsSliceReducer from './invoice/invoice-generators.slice';
// import devToolsEnhancer from 'remote-redux-devtools';
// import Reactotron from '../../ReactotronConfig';
// import {getDefaultMiddleware} from '@reduxjs/toolkit';

// export default configureStore({
//     reducer: {
//         invoiceLabels: invoiceLabelsReducer,
//         invoiceReferences: invoiceReferencesReducer,
//         invoiceCalculations: invoiceCalculationsReducer,
//         invoiceExportOptions: invoiceExportOptionsSliceReducer,
//         invoiceInclusions: invoiceInclusionsSliceReducer,
//         invoiceGenerators: invoiceGeneratorsSliceReducer
//     },
//     // devTools: false,
//     // middleware: [...getDefaultMiddleware({immutableCheck: false})],
//     enhancers: [__DEV__ && require('../../ReactotronConfig').default.createEnhancer()]
//     // enhancers: [Reactotron.createEnhancer()]
//     // enhancers: [devToolsEnhancer()]
// }); 

let store

if(__DEV__){
    store =  configureStore({
       reducer: {
           invoiceLabels: invoiceLabelsReducer,
           invoiceReferences: invoiceReferencesReducer,
           invoiceCalculations: invoiceCalculationsReducer,
           invoiceExportOptions: invoiceExportOptionsSliceReducer,
           invoiceInclusions: invoiceInclusionsSliceReducer,
           invoiceGenerators: invoiceGeneratorsSliceReducer
       },
       // devTools: false,
       // middleware: [...getDefaultMiddleware({immutableCheck: false})],
       enhancers: [__DEV__ && require('../../ReactotronConfig').default.createEnhancer()]
       // enhancers: [Reactotron.createEnhancer()]
       // enhancers: [devToolsEnhancer()]
   }); 

}else{
    store =  configureStore({
        reducer: {
            invoiceLabels: invoiceLabelsReducer,
            invoiceReferences: invoiceReferencesReducer,
            invoiceCalculations: invoiceCalculationsReducer,
            invoiceExportOptions: invoiceExportOptionsSliceReducer,
            invoiceInclusions: invoiceInclusionsSliceReducer,
            invoiceGenerators: invoiceGeneratorsSliceReducer
        },
        // devTools: false,
        // middleware: [...getDefaultMiddleware({immutableCheck: false})],
        // enhancers: [__DEV__ && require('../../ReactotronConfig').default.createEnhancer()]
        // enhancers: [Reactotron.createEnhancer()]
        // enhancers: [devToolsEnhancer()]
    }); 
 
}

export default store;