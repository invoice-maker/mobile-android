import { createSlice, createEntityAdapter, nanoid } from '@reduxjs/toolkit';
import getSymbolFromCurrency from 'currency-symbol-map';

const generateEmptyItem = () => {
    return {
        item_uid: nanoid(),
        item_name: '',
        item_description: '',
        item_qty: 1,
        item_price: null,
        item_discount_type: 'flat',
        item_discount: null,
        // input_container: { //container for fields that have non string values
        //     item_qty: "",
        //     item_price: "",
        //     item_discount: ""
        // },
        item_amount: null
    }
}

export const itemsAdapter = createEntityAdapter({
    selectId: (items) => items.item_uid
})

const emptyInitialState = itemsAdapter.getInitialState({
    edit_mode: false,
    temp_stash: generateEmptyItem(),
    number_format: 'en',
    currency_code: 'USD',
    currency_symbol: '$',
    currency_symbol_on_left: true,
    custom_currency_simbol: null,
    enable_discount_per_item: true,
    charged_items: [],
    calc_stash: {
        global_invoice_discount: {
            type: 'flat',
            enabled: false,
            value: null,
            input_container: ""
        },
        tax: {
            type: 'flat',
            enabled: false,
            value: null,
            input_container: ""
        },
        shipping_cost: {
            taxable: false,
            enabled: false,
            value: null,
            input_container: ""
        },
    },
    invoice_calculation: {
        invoice_subtotal: "",
        global_invoice_discount: {
            type: 'flat',
            enabled: false,
            value: null,
            input_container: ""
        },
        tax: {
            type: 'flat',
            enabled: false,
            value: null,
            input_container: ""
        },
        shipping_cost: {
            taxable: false,
            enabled: false,
            value: null,
            input_container: ""
        },
        balance_due: null
    }
})

const preloadedState = itemsAdapter.upsertOne(emptyInitialState, generateEmptyItem())

const computeAmount = (current_item) => {

    // destructure the objects
    let { item_qty, item_price, item_discount_type, item_discount } = current_item

    // count the total price (qty * price)
    let total_price = (item_qty * item_price)

    // if item discount is not provided, then item amount is equal to total price
    if (!item_discount) {
        current_item.item_amount = total_price
    }

    // calculate if the discount type is percent
    if (item_discount_type === 'percent' && item_discount) {
        current_item.item_amount = total_price * ((100 - item_discount) / 100)
    }

    // calculate if the discount type is flat
    if (item_discount_type === 'flat' && item_discount) {
        current_item.item_amount = total_price - item_discount
    }

    // return the current item
    return current_item
}

const calculateChargedItemsSubTotal = (state) => {
    state.invoice_calculation.invoice_subtotal = state.charged_items.reduce((prev, current) => {
        return prev + current.item_amount
    }, 0)

    return state
}


export const invoiceCalculationsSlice = createSlice({
    name: 'invoiceCalculations',
    initialState: preloadedState,
    reducers: {
        removeItemFromChargedItemByItemUid: (state, action) => {

            const { item_uid } = action.payload;

            state.charged_items = state.charged_items.filter(item => item.item_uid !== item_uid)

            state.temp_stash = generateEmptyItem()

            state.edit_mode = false

            state = calculateChargedItemsSubTotal(state)

            return state

        },
        updateChargedItemByItemId: (state, action) => {

            const { item_uid } = action.payload;

            const itemIndex = state.charged_items.findIndex(item => item.item_uid === item_uid);

            state.charged_items[itemIndex] = state.temp_stash;

            state.temp_stash = generateEmptyItem()

            state.edit_mode = false

            state = calculateChargedItemsSubTotal(state)

            return state

        },
        cancelEditChargedItem: (state, action) => {

            state.temp_stash = generateEmptyItem()

            state.edit_mode = false

            return state
        },
        putChargedItemToTempStashToEdit: (state, action) => {

            const { item_uid } = action.payload

            state.temp_stash = state.charged_items.find(item => item.item_uid === item_uid)

            state.edit_mode = true

            return state
        },
        putItemOnTempStashToChargedItems: (state) => {

            const oldValue = state.temp_stash

            state.charged_items.push(state.temp_stash)

            state.temp_stash = generateEmptyItem()

            state.temp_stash.item_discount_type = oldValue.item_discount_type

            state = calculateChargedItemsSubTotal(state)

            return state
        },
        calculateTempStashAmount: (state) => {

            state.temp_stash = computeAmount(state.temp_stash)

            return state
        },
        setStringValueToTempStash: (state, action) => {
            const { name, value } = action.payload

            state.temp_stash[name] = value

            return state
        },
        setFloatValueToTempStash: (state, action) => {
            const { name, value } = action.payload

            state.temp_stash[name] = parseFloat(value)

            return state
        },
        setValueOnCalcStash:(state,action) => {
            const { name, properties, value } = action.payload

            state.calc_stash[name][properties] = value

            return state
        },
        putItemFromCalcStashToCalculation: (state, action) => {
            const {name} = action.payload

            state.calc_stash[name].enabled = true

            state.invoice_calculation[name] = state.calc_stash[name]

            return state
        },
        removeItemFromCalcStash: (state, action) => {
            const {name} = action.payload

            state.calc_stash[name].enabled = false
            state.calc_stash[name].value = null

            state.invoice_calculation[name] = state.calc_stash[name]

            return state
        },
        // updateGlobalDiscountTypeToCalcStash: (state, action) => {
        //     const { value } = action.payload

        //     state.invoice_calculation.global_invoice_discount.type = value

        //     return state
        // },
        // updateGlobalDiscountValue: (state, action) => {
        //     const { value } = action.payload

        //     state.invoice_calculation.global_invoice_discount.value = value

        //     return state
        // },
        // addGlobalDiscountToCalculation: (state, action) => {

        //     state.invoice_calculation.global_invoice_discount.enabled = true

        //     return state
        // },






        setTaxableShipping: (state, action) => {
            const value = action.payload.value

            state.invoice_calculation.shipping_cost.taxable = value

            return state
        },
        // setEnableDiscountPerItem: (state, action) => {
        //     const value = action.payload.value

        //     state.enable_discount_per_item = value

        //     if (!value) {
        //         Object.entries(state.entities).forEach(entry => {

        //             const [key] = entry

        //             state.entities[key].item_discount = null

        //             state.entities[key] = computeAmount(state.entities[key])

        //         })
        //     }

        //     return state
        // },
        // setLocale: (state, action) => {
        //     const value = action.payload.value

        //     state.locale = value

        //     return state
        // },
        setNumberFormat: (state, action) => {
            const value = action.payload.value

            state.number_format = value

            return state
        },
        setCustomCurrencySymbol: (state, action) => {
            const value = action.payload.value

            state.custom_currency_simbol = value

            return state
        },
        setCurrencySymbolOnLeft: (state, action) => {

            const value = action.payload.value

            state.currency_symbol_on_left = value ? true : false

            return state
        },
        setCurrencyCode: (state, action) => {

            const currency_code = action.payload.value

            state.currency_code = currency_code

            state.currency_symbol = getSymbolFromCurrency(currency_code) || currency_code

            return state
        },

        // togglePredifinedCustomField: (state, action) => {

        //     const { fieldName } = action.payload;

        //     state.invoice_calculation[fieldName].enabled = !state.invoice_calculation[fieldName].enabled

        //     return state
        // },
        calculateSubTotal: (state, action) => {

            state.invoice_calculation.invoice_subtotal = state.charged_items.reduce((prev, current) => {
                return prev + current.item_amount
            }, 0)

            return state
        },
        calculateBalanceDue: (state, action) => {

            const { invoice_subtotal, global_invoice_discount, tax, shipping_cost } = state.invoice_calculation

            let discount
            global_invoice_discount.value = global_invoice_discount.value || 0
            if (global_invoice_discount.type === 'percent' && global_invoice_discount.value) {
                discount = 100 - global_invoice_discount.value
            } else {
                discount = global_invoice_discount.value
            }

            let invoice_tax
            tax.value = tax.value || 0
            if (tax.type === 'percent') {
                invoice_tax = 100 + tax.value
            } else {
                invoice_tax = tax.value
            }

            let total_after_discount
            if (global_invoice_discount.type === 'percent' && global_invoice_discount.value) {
                total_after_discount = invoice_subtotal * (discount / 100)
            } else {
                total_after_discount = invoice_subtotal - discount
            }

            let total_after_taxable_shipping
            shipping_cost.value = shipping_cost.value || 0
            if (shipping_cost.taxable === true) {
                total_after_taxable_shipping = total_after_discount + shipping_cost.value
            } else {
                total_after_taxable_shipping = total_after_discount + 0
            }

            let total_after_tax
            if (tax.type === 'percent') {
                total_after_tax = total_after_taxable_shipping * (invoice_tax / 100)

                // console.log('tax decimal calculation =',(invoice_tax / 100))
                // console.log('sub total after taxable shipping = ',total_after_taxable_shipping)
                // console.log('total after tax calculation = ', total_after_tax )
            } else {
                total_after_tax = total_after_taxable_shipping + invoice_tax
            }

            let total_after_nontaxable_shipping
            if (shipping_cost.taxable === false) {
                total_after_nontaxable_shipping = total_after_tax + shipping_cost.value
            } else {
                total_after_nontaxable_shipping = total_after_tax + 0
            }

            state.invoice_calculation.balance_due = total_after_nontaxable_shipping

            return state

        },
        // setCalculationStringValue: (state, action) => {

        //     const { name, value } = action.payload

        //     state.invoice_calculation[name].type = value

        //     return state
        // },
        // setCalculationFloataValue: (state, action) => {

        //     let { name, value } = action.payload

        //     state.invoice_calculation[name].value = parseFloat(value)

        //     return state
        // },
        // addNewEmptyItem: (state, action) => {

        //     itemsAdapter.upsertOne(state, generateEmptyItem())
        // },
        // setItemStringValue: (state, action) => {

        //     const { item_uid, name, value } = action.payload

        //     state.entities[item_uid][name] = value

        //     state.entities[item_uid] = computeAmount(state.entities[item_uid])

        //     return state
        // },
        // removeItem: (state, action) => {

        //     const { item_uid } = action.payload

        //     delete state.entities[item_uid]

        //     state.ids = state.ids.filter(element => element !== item_uid)

        //     return state
        // },
        // setItemFloatValue: (state, action) => {


        //     let { item_uid, name, value } = action.payload

        //     let current_item = state.entities[item_uid]

        //     current_item[name] = value

        //     state.entities[item_uid] = computeAmount(current_item)

        //     return state
        // }
    }
})

export const {
    setTaxableShipping,
    setCustomCurrencySymbol,
    setCurrencyCode,
    calculateSubTotal,
    // setLocale,
    // setCalculationStringValue,
    // setEnableDiscountPerItem,
    // setItemStringValue,
    // setItemFloatValue,
    // removeItem,
    // addNewEmptyItem,
    // togglePredifinedCustomField,
    // setCalculationFloataValue,
    calculateBalanceDue,
    setCurrencySymbolOnLeft,
    setStringValueToTempStash,
    setFloatValueToTempStash,
    calculateTempStashAmount,
    putItemOnTempStashToChargedItems,
    putChargedItemToTempStashToEdit,
    cancelEditChargedItem,
    updateChargedItemByItemId,
    removeItemFromChargedItemByItemUid,
    setValueOnCalcStash,
    putItemFromCalcStashToCalculation,
    removeItemFromCalcStash,
    setNumberFormat
    // updateGlobalDiscountType,
    // updateGlobalDiscountValue,
    // addGlobalDiscountToCalculation

} = invoiceCalculationsSlice.actions

export const {
    selectAll: selectAllItems,
    selectById: selectItemsByUid } = itemsAdapter.getSelectors(state => state.invoiceItems)

export default invoiceCalculationsSlice.reducer