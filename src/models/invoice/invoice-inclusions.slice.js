import { createSlice } from '@reduxjs/toolkit';

export const invoiceInclusionsSlice = createSlice({
    name: 'invoiceInclusions',
    initialState: {
        payment_terms: false,
        due_date: true,
        ship_to: false,
        item_description: false,
        item_discount: false,
        item_qty: true,
        notes: true,
        terms_and_conditions: true,
        footnote: true,
        greeting_message: true,
        closing_message: true
    },
    reducers: {
        setInclusionValue: (state, action) => {
            const { name, value } = action.payload

            state[name] = value

            return state
        }
    }
})

export const { setInclusionValue } = invoiceInclusionsSlice.actions

export default invoiceInclusionsSlice.reducer
