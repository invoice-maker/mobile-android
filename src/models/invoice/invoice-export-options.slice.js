import { createSlice } from '@reduxjs/toolkit'

export const invoiceExportOptionsSlice = createSlice({
    name: 'invoiceExportOptions',
    initialState: { 
        paper_size: 'A4'
     },
    reducers: {
        setPaperSize: (state, action) => {
            const { value } = action.payload
            state.paper_size = value

            return state
        }
    }
})


export const { setPaperSize } = invoiceExportOptionsSlice.actions


export default invoiceExportOptionsSlice.reducer