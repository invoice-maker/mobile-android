import { createSlice } from '@reduxjs/toolkit'

const DEFAULT_LABELS = {
        business_name_label: 'Your business name', // x
        invoice_issuer_information_content: 'Your business name \nYour email address \nYour street address \nYour phone number \nYour city, state, zip', // x
        bill_to_label: 'Bill To:', // x
        ship_to_label: 'Ship To:', // x
        document_name_label: 'INVOICE', // x
        invoice_number_label: 'Invoice No.', // x
        invoice_date_label: 'Invoice Date', // x
        invoice_due_date_label: 'Due Date', // x
        payment_terms_label: 'Payment Terms', // x
        greeting_message_content: '', // x
        item_description_label: 'Item Description', // x
        item_qty_label: 'Qty', // x
        item_price_label: 'Price', // x
        item_discount_label: 'Discount', // x
        item_amount_label: 'Amount', // x
        subtotal_label: 'Subtotal', // x
        global_discount_label: "Discount",
        tax_label: 'Tax',
        shipping_label: 'Shipping cost',
        amount_paid_label: 'Amount paid',
        balance_due_label: 'Balance due',
        closing_message_content: '', // x
        notes_label: 'Notes', // x
        notes_content: 'Additional info for your customer like bank numbers, your tax number, etc', // x
        terms_and_conditions_label: 'Terms and Conditions', // x
        terms_and_conditions_content: 'Any terms like terms of payment, return policy, revisions policy, etc', // x
        footnote_content: 'This is computer generated invoice no signature required.' // x
}

export const labelsSlice = createSlice({
    name: 'invoiceLabels',
    initialState: { ...DEFAULT_LABELS },
    reducers: {
        setLabel: (state, action) => {
            const { labelName, value } = action.payload
            state[labelName] = value
        }
    }
})

export const { setLabel } = labelsSlice.actions


export default labelsSlice.reducer