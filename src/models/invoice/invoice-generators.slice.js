import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { serverUrl } from '../../configs/configs';
import axios from 'axios';

const generatePDFinvoice = createAsyncThunk(
    'generatePDFInvoice',
    async (payload, thunk) => {

        const response = await axios.post(`${serverUrl}v1/generate-invoice`, payload)
        
        return response.data
    }
)

const invoiceGeneratorsSlice = createSlice({
    name: 'invoiceGenerators',
    initialState: {
        generate_invoice_loading: undefined,
        download_url: null
    },
    reducers: {},
    extraReducers: {
        [generatePDFinvoice.fulfilled]: (state, action) => {

            state.download_url = action.payload.data.download_url
            state.generate_invoice_loading = 'fulfilled'

            return state
        },
        [generatePDFinvoice.pending]: (state, action) => {
            state.generate_invoice_loading = 'pending'
        },
        [generatePDFinvoice.rejected]: (state, action) => {
            // console.log(action.payload)
            state.generate_invoice_loading = 'rejected'
        }
    }
})

export { generatePDFinvoice }

export default invoiceGeneratorsSlice.reducer

