import React, { useState } from 'react';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import store from './src/models/store';
import { Provider } from 'react-redux'
import MainInvoiceSection from './src/screens/invoice/MainInvoiceSection/MainInvoiceSection';
import EditLabels from './src/screens/invoice/EditLabels/EditLabels';
import InvoiceInfo from './src/screens/invoice/InvoiceInfo/InvoiceInfo';
import InvoiceFormats from './src/screens/invoice/InvoiceFormats/InvoiceFormats';
// import BottomSheet from './src/bottom-sheets/BottomSheets';
import { View } from 'react-native';
import HeightContext from './src/context/HeightContext';
import InvoiceLoadingPage from './src/sections/InvoiceLoadingPage';

if (__DEV__) {
  import("./ReactotronConfig")
}

const RootStack = createStackNavigator();

const CreateInvoiceTab = createMaterialTopTabNavigator();

function Craeteinvoice() {
  return (
    <CreateInvoiceTab.Navigator tabBarOptions={{ scrollEnabled: true }}>
      <CreateInvoiceTab.Screen name="Main" component={MainInvoiceSection} />
      <CreateInvoiceTab.Screen name="Invoice Info" component={InvoiceInfo} />
      <CreateInvoiceTab.Screen name="Edit Labels" component={EditLabels} />
      <CreateInvoiceTab.Screen name="Format" component={InvoiceFormats} />
    </CreateInvoiceTab.Navigator>
  )
}

export default function App() {

  const [screenHeight, setScreenHeight] = useState()


  return (
    <Provider store={store}>
      <NavigationContainer>
        <SafeAreaProvider>
          <SafeAreaView style={{ flex: 1, paddingVertical: 0 }}>
            <View style={{ flex: 1 }} onLayout={({ nativeEvent }) => {
              const windowHeight = nativeEvent.layout.height;
              setScreenHeight(windowHeight)
            }}>

              <HeightContext.Provider value={screenHeight}>

                <RootStack.Navigator screenOptions={{ headerShown: false }}>
                
                  <RootStack.Screen name="CreateInvoice" component={Craeteinvoice} />
                  <RootStack.Screen name="InvoiceLoading" component={InvoiceLoadingPage} />
                  {/* <RootStack.Screen name="BottomSheets" component={BottomSheet} /> */}
                </RootStack.Navigator>

              </HeightContext.Provider>
              
            </View>
          </SafeAreaView>
        </SafeAreaProvider>
      </NavigationContainer>
    </Provider>
  );
}
