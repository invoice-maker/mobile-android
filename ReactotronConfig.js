import Reactotron from "reactotron-react-native";
import { reactotronRedux } from 'reactotron-redux'

const reactotron = Reactotron
    .configure({ name: 'Invoice maker' }) // controls connection & communication settings
    .use(reactotronRedux()) //  <- here i am!
    .connect() // let's connect!

export default reactotron